 const convertDateDMY = (value:any) => {
    let date = new Date(value)
    return  date.getDate()+"-"+ (date.getMonth()+1) +"-"+ date.getFullYear()
}
const convertDateYMD = (value:any) => {
    let date = new Date(value)
    return date.getFullYear() +"-"+ (date.getMonth()+1) +"-"+ date.getDate()
}

const DateTimeUtils = {
    convertDateDMY,
    convertDateYMD
}
export default DateTimeUtils

