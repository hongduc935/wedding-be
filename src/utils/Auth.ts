import  CryptoJS from "crypto-js"
import jwt from 'jsonwebtoken'
import { Response, Request } from "express"
import UserModel from '../apis/auth/model/auth.model'

const secretKeyHash1 = "abc"

const generateKey = (email:string,phone:string,serial:string)=>{
    let keyHash = CryptoJS.AES.encrypt(email+phone+serial, secretKeyHash1).toString();
    return keyHash
}

const createAccessToken = (payload:any) => {
    return jwt.sign(payload, `${process.env.JWT_SECRET}`, { expiresIn: '1d' })
}
const createRefreshToken = (payload:any) => {
    return jwt.sign(payload, `${process.env.JWT_REFRESH_TOKEN}`, { expiresIn: '2d' })
}

const generateAccessToken = async (req: Request, res: Response) => {
    try {

        const rf_token = req.cookies.refreshtoken

        if (!rf_token) return res.status(400).json({ msg: "Please login now." })

        jwt.verify(rf_token, process.env.JWT_REFRESH_TOKEN ? process.env.JWT_REFRESH_TOKEN.toString() : "", async (err: any, result: any) => {
            if (err) return res.status(400).json({ msg: "Please login now." })

            const user = await UserModel.findByPk(result.id)

            if (!user) return res.status(400).json({ msg: "This does not exist." })

            const access_token = AuthUtils.createAccessToken({ id: result.id })

            res.status(200).json({
                access_token,
                user
            })
        })

    } catch (err:any) {
        return res.status(500).json({ msg: err.message })
    }
}

const AuthUtils = {
    generateKey,
    createAccessToken,
    createRefreshToken,
    generateAccessToken
}

export default AuthUtils