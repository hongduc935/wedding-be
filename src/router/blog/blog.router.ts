


import express from 'express'
import BlogController from '../../apis/mongoose/blog/blog.controller'

const BlogRouter = express.Router()

BlogRouter.post('/blog/create', BlogController.create)
BlogRouter.post('/blog/mac', BlogController.getBlogMac)
BlogRouter.post('/blog/:slug', BlogController.getBlogBySlug)
// BlogRouter.post('/blog/update/:id', BlogController.updateContentBlog)
export default BlogRouter;
 