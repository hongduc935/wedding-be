import express from 'express'
import CategoryController from '../../apis/category/category.controller'

const CategoryRouter = express.Router()

CategoryRouter.post('/category/create', CategoryController.create)
CategoryRouter.post('/category/update', CategoryController.update)
CategoryRouter.post('/category/bulk-create', CategoryController.bulkCreate) // dùng cho tạo nhiều cùng một lúc
CategoryRouter.post('/category/get-by-id', CategoryController.getById)
CategoryRouter.post('/category/get-all', CategoryController.getAll)
export default CategoryRouter;
