import express from 'express'
import ProductController from '../../apis/product/product.controller'

const ProductRouter = express.Router()

ProductRouter.post('/product/update', ProductController.create)
ProductRouter.post('/product/create', ProductController.create)
ProductRouter.post('/product/create-bulk', ProductController.createBulk)
ProductRouter.post('/product/get-all', ProductController.findAll)
ProductRouter.post('/product/get-all-by-category', ProductController.getProductByCategory)
ProductRouter.post('/product/get-and-condition', ProductController.findAndCondition)
ProductRouter.post('/product/get-or-condition', ProductController.findOrCondition)
ProductRouter.post('/product/get-by-id', ProductController.findById)

ProductRouter.post('/product/category', ProductController.getProductByCategory)


ProductRouter.post('/product/test', ProductController.findProductWithCondition)

export default ProductRouter
