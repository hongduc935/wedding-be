import express from 'express'
import SpuController from '../../apis/spu/spu.controller'

const SpuRouter = express.Router()

SpuRouter.post('/spu/create', SpuController.create)
SpuRouter.post('/spu/create-bulk', SpuController.createBulk)
SpuRouter.post('/spu/get-all', SpuController.findAll)
export default SpuRouter
