import express from 'express'
import RoleController from '../../apis/role/role.controller'

const RoleRouter = express.Router()

RoleRouter.post('/role/get-all', RoleController.findAll)
RoleRouter.post('/role/create-bulk', RoleController.createBulk)
export default RoleRouter
