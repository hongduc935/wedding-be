import express from 'express'
import SkuController from '../../apis/sku/sku.controller'

const SkuRouter = express.Router()

SkuRouter.post('/sku/create', SkuController.create)
SkuRouter.post('/sku/create-bulk', SkuController.createBulk)
SkuRouter.post('/sku/get-one', SkuController.getOne)

export default SkuRouter