import express from 'express'
import AwsSupport from '../../../libs/aws/pre-signed-url'

const AwsRouter = express.Router()

AwsRouter.post('/images/pre-signed-url', AwsSupport.getPresignedUrl)

export default AwsRouter

