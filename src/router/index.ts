import AuthRouter  from  './auth/auth.router'
import CategoryRouter from './category/category.router'
import AttributeRouter from './attribute/attribute.router'
import PartnerRouter from './partner/partner.router'
import ProductRouter from './product/product.router'
import SeoRouter from './seo/seo.router'
import SettingRouter from './setting/setting.router'
import RoleRouter from './role/role.router'
import AssetRouter from './assets/assets.router'
import UserRoleRouter from './user-role/user-role.router'
import SpuRouter from './spu/spu.router'
import SkuRouter from './sku/sku.router'
import ProductAssetRouter from './product-asset/product-asset.router'
import ProductAttributeRouter from './product-attribute/product-attribute.router'
import UserRouter from './auth/user.router'

import AwsRouter from './thirty/aws/aws.router'
import BlogRouter from './blog/blog.router'
const CombineRouter = [
    //Module
    AuthRouter,
    CategoryRouter,
    AttributeRouter,
    PartnerRouter,
    SeoRouter,
    SettingRouter, 
    RoleRouter,
    AssetRouter,
    UserRoleRouter,
    SpuRouter,
    SkuRouter,
    ProductRouter,
    ProductAssetRouter,
    ProductAttributeRouter,
    UserRouter,
    
    // Thirty 
    AwsRouter,
    BlogRouter

]

export default CombineRouter