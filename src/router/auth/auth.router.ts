import express from 'express'
import AuthController from '../../apis/auth/auth.controller'
import AuthMiddleware from '../../middleware/auth'

const AuthRouter = express.Router()

AuthRouter.post(
    '/auth/register',
    // [AuthMiddleware.register],
    AuthController.register
)

AuthRouter.post(
    '/auth/login', 
    // [AuthMiddleware.login],
    AuthController.login
)

AuthRouter.post(
    '/auth/refreshtoken', 
    // [AuthMiddleware.login],
    AuthController.refreshToken
)

export default AuthRouter;
