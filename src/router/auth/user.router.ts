import express from 'express'
import UserController from '../../apis/auth/user.controller'
import AuthMiddleware from '../../middleware/auth'

const UserRouter = express.Router()

UserRouter.post(
    '/user/create-bulk',
    // [AuthMiddleware.register],
    UserController.createBulk
)

UserRouter.get(
    '/user/profile',
    [AuthMiddleware.auth],
    UserController.getProfile
)

export default UserRouter;
