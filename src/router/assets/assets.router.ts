import express from 'express'
import AssetController from '../../apis/assets/assets.controller'
import multer, { diskStorage } from 'multer'
import path from 'path'
import fs from 'fs'
import { Request } from "express"



// Cấu hình Multer để lưu tệp tải lên
const storage = diskStorage({
    destination: (req: Request, file: Express.Multer.File, cb: (error: Error | null, destination: string) => void) => {
      const uploadPath = path.join(__dirname, 'uploads');
      
      if (!fs.existsSync(uploadPath)) {
        fs.mkdirSync(uploadPath, { recursive: true });
      }
      
      cb(null, uploadPath);
    },
    filename: (req: Request, file: Express.Multer.File, cb: (error: Error | null, filename: string) => void) => {
      // Đảm bảo `filename` là một chuỗi
      cb(null, `${Date.now()}-${file.originalname}`);
    }
});
  
const upload = multer({ storage: storage });

const AssetRouter = express.Router()

AssetRouter.post('/asset/get', AssetController.getOne)
AssetRouter.post('/asset/get-one', AssetController.getOne)


AssetRouter.post('/upload', upload.array('files', 10),AssetController.uploadResource)



export default AssetRouter;
