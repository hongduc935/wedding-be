import express from 'express'
import AttributeController from '../../apis/attribute/attribute.controller'

const AttributeRouter = express.Router()

AttributeRouter.post('/attribute/get-all', AttributeController.findAll)
AttributeRouter.post('/attribute/create', AttributeController.create)
AttributeRouter.post('/attribute/get-by-id', AttributeController.findById)
AttributeRouter.post('/attribute/get-and-condition', AttributeController.findAndCondition)
AttributeRouter.post('/attribute/get-or-condition', AttributeController.findOrCondition)
AttributeRouter.post('/attribute/category', AttributeController.findAttributeByCategory)

export default AttributeRouter;
 