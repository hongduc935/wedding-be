import express from 'express'
import PartnerController from '../../apis/partner/partner.controller'

const PartnerRouter = express.Router()

PartnerRouter.post('/partner/create', PartnerController.create)
PartnerRouter.post('/partner/get-all', PartnerController.findAll)
PartnerRouter.post('/partner/get-one', PartnerController.getOne)
PartnerRouter.post('/partner/get-and-condition', PartnerController.findAndCondition)
PartnerRouter.post('/partner/get-or-condition', PartnerController.findOrCondition)
export default PartnerRouter
