import express from 'express'
import ProductAssetController from '../../apis/product-asset/product-asset.controller'

const ProductAssetRouter = express.Router()

ProductAssetRouter.post('/product-asset/create', ProductAssetController.create)
ProductAssetRouter.post('/product-asset/create-bulk', ProductAssetController.createBulk)
ProductAssetRouter.post('/product-asset/update', ProductAssetController.update)
ProductAssetRouter.post('/product-asset/delete', ProductAssetController.delete)
export default ProductAssetRouter
