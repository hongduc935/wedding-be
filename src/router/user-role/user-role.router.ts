import express from 'express'
import UserRoleController from '../../apis/user-role/user-role.controller'

const UserRoleRouter = express.Router()

UserRoleRouter.post('/user-role/create', UserRoleController.create)
UserRoleRouter.post('/user-role/update', UserRoleController.update)
UserRoleRouter.post('/user-role/delete', UserRoleController.delete)

export default UserRoleRouter
