import express from 'express'
import SeoController from '../../apis/seo/seo.controller'

const SeoRouter = express.Router()

SeoRouter.post('/seo/create', SeoController.getOne)
SeoRouter.post('/seo/get-one', SeoController.getOne)

export default SeoRouter
