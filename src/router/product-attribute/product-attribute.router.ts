import express from 'express'
import ProductAttributeController from '../../apis/product-attribute/product-attribute.controller'

const ProductAttributeRouter = express.Router()

ProductAttributeRouter.post('/product-attribute/create', ProductAttributeController.create)
ProductAttributeRouter.post('/product-attribute/create-bulk', ProductAttributeController.createBulk)
ProductAttributeRouter.post('/product-attribute/get-all', ProductAttributeController.findAll)

export default ProductAttributeRouter
