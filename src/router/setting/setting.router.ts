import express from 'express'
import SettingController from '../../apis/setting/setting.controller'

const SettingRouter = express.Router()

SettingRouter.post('/setting/get', SettingController.getOne)
SettingRouter.post('/setting/get-one', SettingController.getOne)

export default SettingRouter
