const validateEmail = (email: string) => {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        )
}
const validatePassword = (password: string) => {
    if (password.length < 6)
        return "Password minlength 6 character"
    return ""
}
const validatePhone = (phone: string) => {
    if (phone.length < 10)
        return "Password minlength 10 character"
    if (phone.length > 11)
        return "Password maxlength 11 character"
    return ""
}

const CombineValidation = {
    validateEmail,
    validatePassword,
    validatePhone
}
export default CombineValidation

