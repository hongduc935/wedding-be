import { v4 as uuidv4 } from 'uuid'

const generateUUID = ()=>{
      return uuidv4();
}

const generateWithCurrentTime = ()=>{
    return new Date().getTime()
}

const GenerateID ={
    generateUUID,
    generateWithCurrentTime
}

export default GenerateID