import schedule from 'node-schedule';
import path from 'path';
import fs from 'fs';

// Đọc tất cả các công việc từ thư mục jobs
const jobsDir = path.join(__dirname, 'jobs');
const jobFiles = fs.readdirSync(jobsDir);

const scheduledJobs = {};

// Lập lịch tất cả các công việc được định nghĩa trong thư mục jobs
jobFiles.forEach(file => {
  if (file.endsWith('.js')) {
    const job = require(path.join(jobsDir, file));
    if (job && job.schedule) {
      const jobName = path.basename(file, '.js');
      scheduledJobs[jobName] = schedule.scheduleJob(job.schedule, job.job);
      console.log(`Scheduled job: ${jobName}`);
    }
  }
});

// Hàm để thêm công việc mới vào lịch
const addJob = (name, scheduleTime, jobFunction) => {
  if (scheduledJobs[name]) {
    console.log(`Job with name ${name} already exists.`);
    return;
  }
  scheduledJobs[name] = schedule.scheduleJob(scheduleTime, jobFunction);
  console.log(`Added new job: ${name}`);
};

// Hàm để hủy công việc
const cancelJob = (name) => {
  if (scheduledJobs[name]) {
    scheduledJobs[name].cancel();
    delete scheduledJobs[name];
    console.log(`Canceled job: ${name}`);
  } else {
    console.log(`No job found with name ${name}`);
  }
};

module.exports = {
  addJob,
  cancelJob,
  scheduledJobs
};
