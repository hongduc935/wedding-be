export const broadcastMessage = (io:any, roomId:any, message:any) => {
    io.to(roomId).emit('broadcast', message);
  };
  
  export const createRoom = (io, roomId) => {
    io.socketsJoin(roomId);
  };
  