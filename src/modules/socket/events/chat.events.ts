import MessagesModel from "../../../apis/mongoose/chat/model/message.model";
import RoomModels from "../../../apis/mongoose/chat/model/room.model";

//Luu trang thai nguoi dung co dang mo connect hay khong 

const chatEvents = (socket: any, io: any,connectedUsers:any) => {
  // Khi người dùng gửi tin nhắn
  socket.on('sendMessage', async ({ roomId, sender, receiver, message, fileUrls = [] }) => {
    // Phát tin nhắn cho tất cả user trong room
    let room = await RoomModels.findOne({ users: { $all: [sender, receiver] } });

    try {

      if (!room) {
        const newRoomId = `room-${sender}-${receiver}`;
        room = new RoomModels({
          roomId: newRoomId,
          users: [sender, receiver],
        });
        await room.save();
      }

      const roomId = room.roomId;


      const newMessage = new MessagesModel({ sender, receiver, content: message, file_url: fileUrls });

      await newMessage.save();

      if (connectedUsers[receiver]){ // Người nhận đang Online và đang nhận tin nhắn lần đầu 
        io.sockets.sockets.get(connectedUsers[receiver])?.join(roomId); 
      }

      // Gửi tin nhắn cho những người đang có mặt trong room realtime 
      io.to(roomId).emit('receiveMessage', { 
        message,
        sender,
        timestamp: new Date(),
      });

    } catch (error) {
      console.error('Error saving message:', error.message); // Log lỗi
    }

  });

  // Người dùng tham gia phòng chat
  socket.on('joinRoom',async ({ roomId, userId }) => {
    // socket.join(roomId);
    // console.log(`${userId} joined room: ${roomId}`);
    // Tải tất cả lích sử tin nhắn 

    const rooms = await RoomModels.find({ users: userId }); //Tìm tất cả các room mà user đó đã chat và join vào room 
    if (rooms.length === 0) {
      console.log(`No rooms found for user: ${userId}`);
    } else {
      // Người dùng join vào tất cả các room đã có trong danh sách
      rooms.forEach(room => {
        socket.join(room.roomId);
        console.log(`${userId} joined room: ${room.roomId}`);

        // Phát sự kiện 'userJoined' cho các user khác trong phòng
        socket.to(room.roomId).emit('userJoined', `${userId} has joined the room.`);
      });
    }
    // Phát cho tất cả mọi người trong phòng
    // socket.to(roomId).emit('userJoined', `${userId} has joined the room.`);
  });

  // Người dùng rời phòng
  socket.on('leaveRoom', ({ roomId }) => {
    socket.leave(roomId);
    console.log(`${socket.id} left room: ${roomId}`);
    socket.to(roomId).emit('userLeft', `${socket.id} has left the room.`);
  });
};

export default chatEvents;
