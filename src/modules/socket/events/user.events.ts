const userEvents = (socket:any, io:any,connectedUsers:any) => {
    // Người dùng đăng nhập vào hệ thống
    socket.on('userLogin', (userData) => {
      console.log(`${userData.username} has logged in.`);
      io.emit('newUser', userData);
    });
  
    // Người dùng ngắt kết nối
    socket.on('disconnect', () => {
      console.log(`${socket.id} disconnected`);
    });
  };
  
  export default userEvents;
  