import { Server as socketIO } from 'socket.io'
import socketConfig from './config/socket.config'
import chatEvents from './events/chat.events'
import userEvents from './events/user.events'
import authMiddleware from './middlewares/auth.middleware'

const connectedUsers:any = {}; 


const initRoomId = [] // Dung để khỏi tạo RoomId của một người khi họ connected đến server Socket

// if()

const initSocket = (server:any) => {
  const io = new socketIO(server, socketConfig);

  // Middleware xác thực người dùng
  io.use(authMiddleware);

  // Sự kiện khi người dùng kết nốif


  io.on('connection', (socket) => {

    const userId = socket.handshake.query.userId; //Phia FE gui den server thong qua user_id login 

    if (!userId) {
      console.log('User ID is missing!');
      return socket.disconnect(); // Ngắt kết nối nếu không có userId
    }

    connectedUsers[`${userId}`] = socket.id; // Luu rang user id dang mo socket 

    console.log(`User connected: ${userId}`);

    console.log(`User connected: ${socket.id}`);

    chatEvents(socket, io, connectedUsers);

    userEvents(socket, io, connectedUsers);

    socket.on('disconnect', () => {
      console.log(`User disconnected: ${socket.id}`); 
      delete connectedUsers[`${userId}`];
    });
  });
};

export default initSocket
