const authMiddleware = (socket:any, next:any) => {
    const token = socket.handshake.auth.token;
  
    // Xác thực token
    if (isValidToken(token)) {
      return next();  // Tiếp tục nếu xác thực thành công
    }
  
    // Trả lỗi nếu xác thực thất bại
    const error:any = new Error("Authentication error");
    error.data = { content: "Please retry later" };
    next(error);
  };
  
  // Hàm kiểm tra token
  const isValidToken = (token) => {
    // Logic kiểm tra token, ví dụ dùng JWT hoặc các phương thức xác thực khác
    return true;
  };
  
  export default authMiddleware;
  