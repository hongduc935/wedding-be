const socketConfig = {
    cors: {
      origin: 'http://localhost:3000',  // Đảm bảo chỉ có frontend của bạn mới kết nối được
      methods: ['GET', 'POST'],
    },
    pingTimeout: 60000,  // Thời gian chờ giữ kết nối
    pingInterval: 25000,  // Khoảng cách giữa các lần ping
  };
  
  export default socketConfig;
  