import express from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';

const routerUpload = express.Router();

// Cấu hình thư mục lưu trữ file upload
const uploadDir = path.join(__dirname, '../uploads');

// Tạo thư mục uploads nếu chưa tồn tại
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

// Cấu hình Multer
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, uploadDir); // Đường dẫn lưu file
  },
  filename: (req, file, cb) => {
    // Tạo tên file với timestamp để tránh trùng lặp
    cb(null, Date.now() + path.extname(file.originalname));
  }
});

const upload = multer({ storage });

// Định nghĩa route để xử lý file upload
routerUpload.post('/upload', upload.single('file'), (req, res) => {
  try {
    res.status(200).json({
      message: 'File uploaded successfully!',
      file: req.file
    });
  } catch (error) {
    res.status(500).json({
      message: 'File upload failed!',
      error
    });
  }
});

export default routerUpload
