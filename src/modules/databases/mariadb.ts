import { Sequelize } from 'sequelize'

const sequelize = new Sequelize(
  'apple', // Tên cơ sở dữ liệu
  'newuser', // Tên người dùng cơ sở dữ liệu
  'password', // Mật khẩu cơ sở dữ liệu
  // 'root', // Tên người dùng cơ sở dữ liệu
  // '12345678Aa@', // Mật khẩu cơ sở dữ liệu 
  {
    host: 'localhost', // Địa chỉ host của cơ sở dữ liệu
    port: 3306, // Port kết nối cơ sở dữ liệu (MariaDB thường dùng port 3306)
    dialect: 'mariadb', // Loại cơ sở dữ liệu
    pool: {
      max: 5, // Số lượng connection tối đa trong pool
      min: 0, // Số lượng connection tối thiểu trong pool
      acquire: 30000, // Thời gian tối đa để một connection được lấy từ pool (miliseconds)
      idle: 10000, // Thời gian tối đa mà một connection có thể ở trong pool khi rãnh rỗi (miliseconds)
    },
    logging: false // Tắt logging nếu không muốn hiển thị log của Sequelize
  }
);

export default sequelize