import nodemailer from 'nodemailer';

// Tạo transporter
const Transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.EMAIL_USER, // Email của bạn từ biến môi trường
    pass: process.env.EMAIL_PASS  // Mật khẩu email từ biến môi trường
  }
});

// Kiểm tra kết nối email
Transporter.verify((error, success) => {
  if (error) {
    console.log('Email configuration error:', error);
  } else {
    console.log('Email server is ready to take messages');
  }
});

export default Transporter;
