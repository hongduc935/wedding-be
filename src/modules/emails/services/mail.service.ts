

import Transporter from '../configs/mail.config';
import {REGISTER_ACCOUNT,RESET_ACCOUNT} from '../templates/auth.template';


const RegisterAccount = async (to:string, user_name:string) => {
  try {
    const mailOptions = {
      from: process.env.EMAIL_USER, 
      to, 
      subject: 'Welcome to Our Service!',
      text: `Welcome ${user_name}!`,
      html: REGISTER_ACCOUNT(user_name)
    };

    const info = await Transporter.sendMail(mailOptions);
  } catch (error) {
    throw new Error('Email sending failed');
  }
};

const ResetAccount = async (to:string, user_name:string) => {
    try {
      const mailOptions = {
        from: process.env.EMAIL_USER, 
        to, 
        subject: 'Welcome to Our Service!',
        text: `Welcome ${user_name}!`,
        html: RESET_ACCOUNT(user_name)
      };
  
      const info = await Transporter.sendMail(mailOptions);
    } catch (error) {
      throw new Error('Email sending failed');
    }
  };


const MailServices = {
    RegisterAccount,
    ResetAccount
}

export default MailServices
