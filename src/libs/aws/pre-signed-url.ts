import  AWS from 'aws-sdk'
import { Request ,Response} from 'express'

const s3 = new AWS.S3()

const getPresignedUrl = (req:Request, res:Response) => {
    const { fileName, fileType } = req.body
  
    const params = {
      Bucket: process.env.AWS_BUCKET_NAME,
      Key: fileName,
      Expires: 60, // Thời gian hết hạn của URL (tính bằng giây)
      ContentType: fileType,
      ACL: 'public-read', // Hoặc quyền truy cập bạn muốn
    };
  
    s3.getSignedUrl('putObject', params, (err, url) => {
      if (err) {
        return res.status(500).json({ error: err.message })
      }
      res.json({ url })
    });
  }

const AwsSupport = {
    getPresignedUrl
}
export default AwsSupport