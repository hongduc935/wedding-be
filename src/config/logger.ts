import { Service } from 'typedi'
import { createLogger, format, transports, Logger as WinstonLogger } from 'winston'

@Service()
class Logger {
    private logger: WinstonLogger

    constructor() {
        this.logger = createLogger({
            level: 'info',
            format: format.combine(
                format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                format.errors({ stack: true }),
                format.splat(),
                format.json()
            ),
            // defaultMeta: { service: 'your-service-name' },
            transports: [
                new transports.File({ filename: 'error.log', level: 'error' }),
                new transports.File({ filename: 'combined.log' })
            ]
        });

        if (process.env.NODE_ENV !== 'production') {
            this.logger.add(new transports.Console({
                format: format.combine(
                    format.colorize(),
                    format.simple()
                )
            }));
        }
    }

    log(level: string, message: string, meta?: any): void {
        this.logger.log(level, message, meta);
    }

    info(message: string, meta?: any): void {
        this.log('info', message, meta);
    }

    warn(message: string, meta?: any): void {
        this.log('warn', message, meta);
    }

    error(message: string, meta?: any): void {
        this.log('error', message, meta);
    }

    debug(message: string, meta?: any): void {
        this.log('debug', message, meta);
    }
}

export default Logger
