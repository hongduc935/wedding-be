import Logger from './logger'

function wrapWithLogging(cls: any, methodName: string) {
    const originalMethod = cls[methodName]
    const logger = new Logger()

    cls[methodName] = function (...args: any[]) {
        const startTime = performance.now()

        try {
            const result = originalMethod.apply(this, args)

            if (result instanceof Promise) {
                return result.then((res) => {
                    const endTime = performance.now()
                    logger.info(`[METHOD]: ${methodName} [STATUS]: Successfully completed  [TIME]: ${endTime - startTime} ms.`)
                    return res
                }).catch((error) => {
                    logger.error(`Error in ${methodName}: ${error}`)
                });
            } else {
                const endTime = performance.now()
                logger.info(`[METHOD] : ${methodName} [STATUS]: Successfully completed [TIME]: ${endTime - startTime} ms.`)
                return result
            }
        } catch (error) {
            logger.error(`Error in ${methodName}: ${error}`)
            throw error; 
        }
    };
}

export default wrapWithLogging
