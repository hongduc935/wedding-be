import  { Model, DataTypes } from 'sequelize';
import { v4 as uuidv4 } from 'uuid'
import sequelize from '../../../modules/databases/mariadb';

class AttributeModel extends Model {
    public attribute_id!: string;
    public attribute_name!: string;
    public category_id!: number;
    public attribute_value!: string;
    public created_at!: string;
    public updated_at!: string;
}
// ID: 1 , SSD : MAC : 128
// ID: 2 , RAM : MAC : 8
// ID: 3 , CPU : MAC : M1

// ID: 3 , RAM : IPHONE : 6
// ID: 4 , RAM : IPHONE : 12


AttributeModel.init({
    attribute_id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        primaryKey: true,
        field: 'ATTRIBUTE_ID'
    },
    attribute_name: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'ATTRIBUTE_NAME'
    },
    attribute_value: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'ATTRIBUTE_VALUE'
    },
    category_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        field: 'CATEGORY_ID'
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'CREATED_AT', 
    } ,
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'UPDATED_AT', 
    }
}, {
    sequelize,
    modelName: 'AttributeModel',
    tableName: 'ATTRIBUTES',
    timestamps: false,
    underscored: true
});



export default AttributeModel;
