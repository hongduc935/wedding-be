import  { Service } from 'typedi'
import  {AttributeModel} from '../../index'
import { Op } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'

@Service()
class AttributeService {

  async create(data:any) {
    return await AttributeModel.create(data)
  }

  async createBulk(data:any[]) {
    return await AttributeModel.bulkCreate(data)
  }

  async findAtributeByCategory(category_id:number) {
  //   return  await AttributeModel.findAll({
  //     where: { category_id: category_id },
  //     attributes: ['attribute_name', [
  //       sequelize.fn('GROUP_CONCAT', 
  //       sequelize.col('attribute_value')), 'attribute_values']
  //     ],
  //     group: ['attribute_name']
  // });

  return await AttributeModel.findAll({
      where: { category_id: category_id },
      attributes: ['attribute_id', 'attribute_name', 'attribute_value'],
      order: ['attribute_name']
  });

    // return await AttributeModel.findByPk()
  }

  async findAll() {
    return await AttributeModel.findAndCountAll()
  }

  async findById(id: string) {
    return await AttributeModel.findByPk(id)
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await AttributeModel.findByPk(id)
    if (user) {
      return await user.update(data)
    }
    throw new Error('Attribute not found')
  }

  async delete(id: string) {
    const user = await AttributeModel.findByPk(id)
    if (user) {
      return await user.destroy()
    }
    throw new Error('Attribute not found')
  }

  async findAndCondition(condition: { [key: string]: any }) {
    return await AttributeModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findOrCondition(condition: { [key: string]: any }) {
    return await AttributeModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default AttributeService;
