import { Container }from 'typedi'
import { Response, Request } from 'express';
import AttributeService from './service/attribute.service'
import { Service } from 'typedi';
import ResponseConfig from '../../config/response-http'
const attributeServiceInstance = Container.get(AttributeService)

@Service()
class AttributeController {


    async findAttributeByCategory  (req:Request, res:Response) {
        try {
            const {category_id} = req.body 
            const records = await attributeServiceInstance.findAtributeByCategory(Number(category_id));
            const attributesMap = {};
            records.forEach(record => {
                if (!attributesMap[record.attribute_name]) {
                    attributesMap[record.attribute_name] = {
                        attribute_name: record.attribute_name,
                        attribute_values: []
                    };
                }
                attributesMap[record.attribute_name].attribute_values.push({
                    attribute_id: record.attribute_id,
                    value: record.attribute_value
                });
            });

            // Chuyển đổi object map thành mảng kết quả
            const result = Object.values(attributesMap);

            if(!records){
                return new ResponseConfig(2000,result,'Not Found Record ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,result,'Have Record ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        } 
    }

    async create(req:Request, res:Response) {
        try {
            const record = await attributeServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'Attribute was Create Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }

    async createBulk(req:Request, res:Response) {
        try {
            const record = await attributeServiceInstance.createBulk(req.body)
            return new ResponseConfig(1000,record ,'Attribute was Create Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }

    async findAll(req:Request, res:Response) {
        try {
            const result = await attributeServiceInstance.findAll()
            return new ResponseConfig(1000,result,'Get All Attribute Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }

    async findById  (req:Request, res:Response) {
        try {
            const record = await attributeServiceInstance.findById(req.body.id)
            if(!record){
                return new ResponseConfig(2000,record,'Record Not Found ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,record,'Get Attribute by id successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAndCondition  (req:Request, res:Response) {
        try {
            const records = await attributeServiceInstance.findAndCondition(req.body);
            if(!records){
                return new ResponseConfig(2000,records,'Not Found Record ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,records,'Have Record ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        }
    }
    async findOrCondition (req:Request, res:Response) {
        try {
            const records = await attributeServiceInstance.findOrCondition(req.body);
            if(!records){
                return new ResponseConfig(2000,records,'Not Found Record ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,records,'Have Record ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        }
    }
    async getOne  (req:Request, res:Response)  {
        try {
          const newUser = await attributeServiceInstance.create(req.body);
          res.status(201).json({ message: 'User created successfully', data: newUser });
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update  (req:Request, res:Response) {
        try {
            const newUser = await attributeServiceInstance.create(req.body);
            res.status(201).json({ message: 'User created successfully', data: newUser });
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request , res:Response)  {
        try {
            const newUser = await attributeServiceInstance.delete(req.body.id);
            res.status(201).json({ message: 'User created successfully', data: newUser });
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AttributeController ] : [getByCondition]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new AttributeController()



