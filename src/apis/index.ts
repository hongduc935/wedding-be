import CategoryModel from './category/model/category.model';
import AttributeModel from './attribute/model/attribute.model';
import AssetModel from './assets/model/assets.model';
import SpuModel from './spu/model/spu.model';
import SkuModel from './sku/model/sku.model';
import ProductModel from './product/model/product.model';
import UserModel from './auth/model/auth.model';
import RoleModel from './role/model/role.model';
import UserRoleModel from './user-role/model/user-role.model';
import ProductAssetModel from './product-asset/model/product-asset.model';
import ProductAttributeModel from './product-attribute/model/product-attribute.model';
import OrderModel from './order/model/order.model';
import OrderItemModel from './order-item/model/order-item.model';
import PartnerModel from './partner/model/partner.model';
import SeoModel from './seo/model/seo.model';
import SettingModel from './setting/model/setting.model';

// Thiết lập quan hệ giữa SPU và SKU
SpuModel.hasMany(SkuModel, { foreignKey: 'spu_id' });
SkuModel.belongsTo(SpuModel, { foreignKey: 'spu_id' });

// Quan hệ giữa SPU và Category
SpuModel.belongsTo(CategoryModel, { foreignKey: 'category_id' });
CategoryModel.hasMany(SpuModel, { foreignKey: 'category_id' });

// Quan hệ giữa Product và SPU
// ProductModel.belongsTo(SkuModel, { foreignKey: 'sku_id' });
ProductModel.belongsTo(SkuModel, { foreignKey: 'sku_id' , as: 'sku'});


// Quan hệ giữa User và Product
UserModel.hasMany(ProductModel, { foreignKey: 'created_by' });
ProductModel.belongsTo(UserModel, { foreignKey: 'created_by' });

// Quan hệ giữa ProductAsset và Product
ProductAssetModel.belongsTo(ProductModel, { foreignKey: 'product_id' });
ProductModel.hasMany(ProductAssetModel, { foreignKey: 'product_id' });

// Quan hệ giữa ProductAsset và Asset
ProductAssetModel.belongsTo(AssetModel, { foreignKey: 'asset_id' });
AssetModel.hasMany(ProductAssetModel, { foreignKey: 'asset_id' });

// Quan hệ giữa ProductAttribute và Product
ProductAttributeModel.belongsTo(ProductModel, { foreignKey: 'product_id' });
ProductModel.hasMany(ProductAttributeModel, { foreignKey: 'product_id', as: 'attributes' });

// Quan hệ giữa ProductAttribute và Attribute
ProductAttributeModel.belongsTo(AttributeModel, { foreignKey: 'attribute_id' });
AttributeModel.hasMany(ProductAttributeModel, { foreignKey: 'attribute_id' });

// Quan hệ giữa UserRole và User
UserRoleModel.belongsTo(UserModel, { foreignKey: 'user_id' });
UserModel.hasMany(UserRoleModel, { foreignKey: 'user_id' });

// Quan hệ giữa UserRole và Role
UserRoleModel.belongsTo(RoleModel, { foreignKey: 'role_id' });
RoleModel.hasMany(UserRoleModel, { foreignKey: 'role_id' });

// Quan hệ giữa OrderItem và Order
OrderItemModel.belongsTo(OrderModel, { foreignKey: 'order_id' });
OrderModel.hasMany(OrderItemModel, { foreignKey: 'order_id' });

// Quan hệ giữa OrderItem và Product
OrderItemModel.belongsTo(ProductModel, { foreignKey: 'product_id' });
ProductModel.hasMany(OrderItemModel, { foreignKey: 'product_id' });

export {
    CategoryModel,
    AttributeModel,
    AssetModel,
    SpuModel,
    SkuModel,
    ProductModel,
    UserModel,
    RoleModel,
    UserRoleModel,
    ProductAssetModel,
    ProductAttributeModel,
    OrderModel,
    OrderItemModel,
    PartnerModel,
    SeoModel,
    SettingModel
};
