import  { Model, DataTypes } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import { v4 as uuidv4 } from 'uuid'
import SpuModel from '../../spu/model/spu.model'

class SkuModel extends Model {
    public sku_id!: string;
    public sku_code!: string; // Mã Sản phẩm 
    public spu_id!: string; // thuộc nhóm sỏn phẩm nào 
    public price!: number; // giá bán 
    public quantity!:number ; // số lượng đang bán 
    public attributes!: string; // thống số của sản phẩm 
    public created_at!: string;
    public updated_at!: string;
    public deleted_at!: string;
}

SkuModel.init({
    sku_id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        primaryKey: true,
        field: 'SKU_ID', 
    },
    spu_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
            model: SpuModel,
            key: 'spu_id'
        },
        field: 'SPU_ID'
    },
    sku_code: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: 'SKU_CODE'
    },
    price: {
        type: DataTypes.DECIMAL(10, 2),
        allowNull: false,
        field: 'PRICE'
    },
    quantity: {
        type: DataTypes.BIGINT,
        allowNull: false,
        field: 'QUANTITY'
    },
    attributes: {
        type: DataTypes.JSON,
        allowNull: true,
        field: 'ATTRIBUTES'
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'CREATED_AT'
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'UPDATED_AT'
    },
    deleted_at:{
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'DELETED_AT'
    }
}, {
    sequelize,
    modelName: 'SkuModel',
    tableName: 'SKU',
    underscored: true,
    timestamps: false,
})

export default SkuModel 
