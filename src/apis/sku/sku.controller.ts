import  { Container } from 'typedi'
import { Response, Request } from 'express'
import SkuService from './service/sku.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'


const skuServiceInstance = Container.get(SkuService)

@Service()
class SkuController {

    async create  (req:Request, res:Response)  {
        try {
          const record = await skuServiceInstance.create(req.body)
          return new ResponseConfig(1000,record,'Create Bulk SKU successfully.').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SkuController ] : [ create ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk  (req:Request, res:Response)  {
        try {
            const {data} = req.body
            const records = await skuServiceInstance.createBulk(data)
            return new ResponseConfig(1000,records,'Create Bulk SKU successfully.').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SkuController ] : [ create ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getOne  (req:Request, res:Response)  {
        try {
          const record = await skuServiceInstance.findUserByAndCondition(req.body)
          return new ResponseConfig(1000,record,'[getOne] SKU successfully.').ResponseSuccess(req,res)
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ SkuController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update  (req:Request, res:Response)  {
        try {
            const record = await skuServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'[update] SKU successfully.').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SkuController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request, res:Response)  {
        try {
            const record = await skuServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'[delete] SKU successfully.').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SkuController ] : [ delete ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new SkuController()



