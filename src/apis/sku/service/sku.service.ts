import  { Service } from 'typedi'
import  {SkuModel} from '../../index'
import { Op } from 'sequelize'



@Service()
class SkuService {

  async create(data:any) {
    return await SkuModel.create(data)
  }

  async createBulk(data:any) {
    return await SkuModel.bulkCreate(data)
  }

  async findAll() {
    return await SkuModel.findAndCountAll()
  }


  async getById(id: number) {
    return await SkuModel.findByPk(id)
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await SkuModel.findByPk(id)
    if (user) {
      return await user.update(data)
    }
    return null 
  }

  async delete(id: number) {
    const user = await SkuModel.findByPk(id);
    if (user) {
      return await user.destroy();
    }
    return null 
  }

  async findUserByAndCondition(condition: { [key: string]: any }) {
    return await SkuModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findUserByOrCondition(condition: { [key: string]: any }) {
    return await SkuModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default SkuService;
