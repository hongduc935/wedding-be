import  { Service } from 'typedi'
import  {PartnerModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class PartnerService {
  async create(data:any) {
    return await PartnerModel.create(data)
  }

  async getById(id: number) {
    return await PartnerModel.findByPk(id)
  }

  async findAll() {
    return await PartnerModel.findAndCountAll()
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await PartnerModel.findByPk(id);
    if (user) {
      return await user.update(data)
    }
    throw new Error('Item not found')
  }

  async delete(id: number) {
    const user = await PartnerModel.findByPk(id)
    if (user) {
      return await user.destroy()
    }
    throw new Error('Item not found')
  }

  async findAndCondition(condition: { [key: string]: any }) {
    return await PartnerModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findOrCondition(condition: { [key: string]: any }) {
    return await PartnerModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default PartnerService
