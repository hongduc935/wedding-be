import { Model, DataTypes } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'

class PartnerModel extends Model {
    public partner_id!: string // khóa chính 
    public product_id!: string // id của product 
    public address !: string // thuộc tính id 
    public status_register !: string // giá trị của thuộc tính ví dụ thuộc tính màu sắc có giá trị là xanh 
    public created_at !: Date
    public updated_at !: Date 
}

PartnerModel.init({
    partner_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'PARTNER_ID' 
    },
    product_id: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'PARTNER_NAME'
    },
    address:{
        type: DataTypes.STRING,
        allowNull: false,
        field: 'ADDRESS'
    },
    status_register:{
        type: DataTypes.STRING,
        allowNull: false,
        field: 'STATUS_REGISTER'
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'CREATED_AT'
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'UPDATED_AT'
    }
}, {
    sequelize,
    modelName: 'PartnerModel',
    tableName: 'PARTNER',
    timestamps: false,
    underscored: true
});

export default PartnerModel;
