import { Container } from 'typedi'
import { Response, Request } from 'express'
import PartnerService from './service/partner.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'

const partnerServiceInstance = Container.get(PartnerService)

@Service()
class PartnerController {

    async create  (req:Request, res:Response) {
        try {
            const record = await partnerServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'Get Partner successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAll (req:Request, res:Response) {
        try {
            const record = await partnerServiceInstance.findAll()
            return new ResponseConfig(1000,record,'Get Partner successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAndCondition  (req:Request, res:Response) {
        try {
            const record = await partnerServiceInstance.findAndCondition(req.body)
            return new ResponseConfig(1000,record,'Get Partner successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ findAndCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findOrCondition  (req:Request, res:Response) {
        try {
            const record = await partnerServiceInstance.findOrCondition(req.body)
            return new ResponseConfig(1000,record,'Get Partner successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ findOrCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getOne  (req:Request, res:Response)  {
        try {
            const record = await partnerServiceInstance.create(req.body);
            return new ResponseConfig(1000,record,'Get Attribute by id successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update  (req:Request, res:Response) {
        try {
            const record = await partnerServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'Get Attribute by id successfully').ResponseSuccess(req,res)
        } catch (err:any) {
            return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request , res:Response)  {
        try {
            const record = await partnerServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'Get Attribute by id successfully').ResponseSuccess(req,res)
        } catch (err:any) {
            return new ResponseConfig(2000,null,'Error system in [ PartnerController ] : [ delete ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new PartnerController()



