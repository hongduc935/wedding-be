

interface AssetDTO {
    asset_id:String
    asset_name:String
}

export interface ProductDto{
    product_name:string
    sku_id:string
    description:string
    manufacturer:string
    weight:string
    quantity:string
    product_price:string
    dimensions:string
    product_status:string
    created_by:Date
    product_condition:string
}
export default AssetDTO