import  { Container } from 'typedi'
import { Response, Request } from 'express'
import ProductService from './service/product.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'
import SkuService from '../sku/service/sku.service'
import { Op, Sequelize } from 'sequelize'
import ProductAttributeModel from '../product-attribute/model/product-attribute.model'
import ProductModel from './model/product.model'
import AttributeModel from '../attribute/model/attribute.model'
import ProductAssetService from '../product-asset/service/product-asset.service'
import { ProductDto } from './dto/product.dto'
import sequelize from '../../modules/databases/mariadb'


const productServiceInstance = Container.get(ProductService)
const skuServiceInstance = Container.get(SkuService)
const productAssetServiceInstance = Container.get(ProductAssetService)
@Service()
class ProductController {

    async findAll (req:Request, res:Response) {
        try {
            const record = await productServiceInstance.findAll()
            return new ResponseConfig(1000,record,'Get Partner successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ findAll ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getProductByCategory (req:Request, res:Response) {
        try {
            const {category_name} = req.body 
            const record = await productServiceInstance.getProductList(category_name)
            return new ResponseConfig(1000,record,'Get Partner successfully').ResponseSuccess(req,res)
        } catch (err:any) {
          return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ findAll ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findById  (req:Request, res:Response)  {
        try {
            const {id} = req.body
            const records = await productServiceInstance.findById(id)

            console.log("Product Detail :" + JSON.stringify(records))
            if(!records){
                return new ResponseConfig(2000,records,'Not Found Record').ResponseSuccess(req,res)
            }

            return new ResponseConfig(1000,records,'Get records successfully').ResponseSuccess(req,res)
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAndCondition  (req:Request, res:Response)  {
        try {

            const conditions = {
                manufacturer: 'Apple',
                // weight: { [Op.gt]: 1 }, // Ví dụ: Tìm các sản phẩm có trọng lượng lớn hơn 1
                // dimensions: '13x9x0.6'
            };

            const records = await productServiceInstance.findAndCondition(conditions)

            if(!records){
                return new ResponseConfig(2000,records,'Not Found Record').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,records,'Get records successfully').ResponseSuccess(req,res)
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findProductWithCondition(req: Request, res: Response) {
        try {
            const reqData = req.body;

            const page = parseInt(req.query.page as string) || 1; 
            const limit = parseInt(req.query.limit as string) || 10; 
    
            const offset = (page - 1) * limit;

            const ids = Object.values(reqData).filter(value => value !== null && value !== '');
    
            let products;
    
            if (ids.length > 0) {
                const productIds = await ProductAttributeModel.findAll({
                    where: { attribute_id: ids },
                    attributes: ['product_id', [sequelize.fn('COUNT', sequelize.col('attribute_id')), 'count']],
                    group: 'product_id',
                    having: sequelize.where(sequelize.fn('COUNT', sequelize.col('attribute_id')), '=', ids.length),
                    order: [['product_id', 'ASC']],
                    raw: true
                });
                const productIdsArray = productIds.map(item => item.product_id);
    
                products = await ProductModel.findAll({
                    where: { product_id: { [Op.in]: productIdsArray } },
                    include: [{
                        model: ProductAttributeModel,
                        as: 'attributes',
                        attributes: ['attribute_id', 'attribute_value']
                    }],
                    offset: offset,
                    limit: limit
                });
            } 
            else {
                // Trường hợp không có ids nào, truy vấn tất cả sản phẩm
                products = await ProductModel.findAll({
                    include: [{
                        model: ProductAttributeModel,
                        as: 'attributes',
                        attributes: ['attribute_id', 'attribute_value']
                    }],
                    offset: offset,
                    limit: limit
                });
            }
    
            console.log("Product query Res: " + JSON.stringify(products));
            return new ResponseConfig(1000, products, 'Get All Product').ResponseSuccess(req, res);
        } catch (err) {
            console.error("Error: " + err.message);
            return new ResponseConfig(2000, null, 'Error system in [ ProductController ] : [ getByCondition ]' + err.message).ResponseSuccess(req, res);
        }
    }
    
    
    async findOrCondition  (req:Request, res:Response)  {
        try {
            const records = await productServiceInstance.findOrCondition(req.body)
            if(!records){
                return new ResponseConfig(2000,records,'Not Found Record').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,records,'Get records successfully').ResponseSuccess(req,res)
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async create  (req:Request, res:Response)  {
        try {
        
            // const record ="resr"
            console.log("Record Product : "+ JSON.stringify(req.body))

            const {
                product_name,
                manufacturer,
                weight,
                dimensions,
                sku_code,
                spu_id,
                price,// Giá bán của khách hàng ,
                product_condition,
                quantity,
                description,
                created_by
            } = req.body

            // Tìm SKU 
            let recordSku = await skuServiceInstance.findUserByAndCondition({sku_code})

            // Kiểm tra SKU có tồn tại không 
            if(!recordSku){
                // recordSku = await skuServiceInstance.create(newSku)
                console.log("Not Found SKU . Please Contact Admin .")
            }
            // Tạo Product Schema
            const newProduct :ProductDto = {
                product_name,
                sku_id:recordSku.sku_id,
                description,
                manufacturer,
                weight,
                quantity,
                product_price:price,
                dimensions,
                product_status:"CHUABAN",
                created_by,
                product_condition
            }

          

            const record = await productServiceInstance.create(newProduct)

            const attribute_product = [{
                attribute_id:"",
                product_id:record.product_id
            }]

            // Tạp danh sách các hình ảnh của product 
            const newAsset = [{
                file_name:"",
                path: "",
                type: "IMAGE",
                size: "500",
                module_name: "PRODUCT",
                module_id : record.product_id,
                created_at: new Date() ,
                updated_at: new Date(),
                deleted_at:new Date()
            }]

            const product_asset  = [{
                product_id:record.product_id,
                asset_id:"asset-001",
                type:"Primary"
            }]

            for(const item of product_asset){
                await productAssetServiceInstance.create(item)
            }
            

            return new ResponseConfig(1000,record,'Create Product records successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk  (req:Request, res:Response)  {
        try {
            const {data} = req.body
            const record = await productServiceInstance.createBulk(data)
            return new ResponseConfig(1000,record,'Create Product records successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update  (req:Request, res:Response)  {
        try {
            const newUser = await productServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request, res:Response)  {
        try {
            const newUser = await productServiceInstance.create(req.body);
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ delete ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new ProductController()



