import  { Service } from 'typedi'
import  {CategoryModel, ProductModel, SkuModel} from '../../index'
import { Op } from 'sequelize'
import SpuModel from '../../../apis/spu/model/spu.model'
@Service()
class ProductService {

  async create(data:any) {
    return await ProductModel.create(data);
  }

  async createBulk(data:any) {
    return await ProductModel.bulkCreate(data);
  }

  async findById(id: string) {
    return await ProductModel.findByPk(id, {
      include: [
          // {
          //     model: CategoryModel,
          //     attributes: ['CATEGORY_NAME'], // Chỉ lấy cột cần thiết
          // },
          {
              model: SkuModel,
              as: 'sku',
              attributes: ['sku_code', 'price', 'quantity'],
          },
      ],
  });
  }

  async findAll() {
    return await ProductModel.findAndCountAll()
  }

  async getProductList(category_name:string) {
    try {
      const products = await ProductModel.findAll({
        include: [
          {
            model: SpuModel,
            // as: 'spu',
            attributes: ['spu_code', 'product_name', 'description'],
            required: true, 
            include: [
              {
                model: CategoryModel,
                as: 'category',
                attributes: ['category_name'],
                where: { category_name: category_name},
                required: true, 
              }
            ]
          },
        ],
        attributes: ['product_id', 'product_name', 'description', 'manufacturer', 'weight', 'dimensions', 'created_at', 'updated_at'],
      });

      return products;
    } catch (error) {
      console.error('Error fetching product list:', error);
      throw error;
    }
  }

  async findByCategory() {
    return await ProductModel.findAll({
      limit: 2, // giới hạn trả về
      offset: 3, // page thứ 
      where: {

      }, // conditions
    })
  }


  async findPagination() {
    return await ProductModel.findAll({
      limit: 2, // giới hạn trả về
      offset: 3, // page thứ 
      where: {}, // conditions
    })
  }
  async update(id: number, data: { username?: string; email?: string }) {
    const user = await ProductModel.findByPk(id);
    if (user) {
      return await user.update(data);
    }
    throw new Error('User not found');
  }

  async delete(id: number) {
    const user = await ProductModel.findByPk(id);
    if (user) {
      return await user.destroy();
    }
    throw new Error('User not found');
  }

  async findAndCondition(condition: { [key: string]: any }) {
    return await ProductModel.findAll({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findOrCondition(condition: { [key: string]: any }) {
    return await ProductModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default ProductService;
