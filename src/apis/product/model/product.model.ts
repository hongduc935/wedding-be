import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import { v4 as uuidv4 } from 'uuid'
import moment from 'moment';
import UserModel from '../../auth/model/auth.model';
import SkuModel from '../../sku/model/sku.model';


class ProductModel extends Model {

  public product_id!: string
  public product_name!: string
  public sku_id!: string
  public description!: string // Mô tả riêng của người bán 
  public created_by!:string
  public product_status!:string // Tình trạng sản phẩm 'Đã Bán' ,'Chưa bán', 'Đợi'
  public product_condition!:string  // Tình trạng mới hoặc cũ 
  public product_price!: number // GIÁ TIỀN KHI LÀ TÌNH TRẠNG CŨ  . Mỗi khách hàng sẽ có mức giá bán khác nhau.
  public manufacturer!: string// Hãng sản xuất 
  public weight!: number// Cân nặng 
  public quantity!: number // số lượng sản phẩm đang bán nếu là hàng cũ
  public dimensions!: string // Kích thước sản phẩm 
  public created_at!: Date // Ngày đăng sản phẩm 
  public updated_at!: Date // Ngày chỉnh sửa gần nhất 
  public deleted_at!: Date | null // Ngày xoá nếu có 

  get formattedCreatedAt() {
    return moment(this.created_at).format('YYYY-MM-DD HH:mm:ss')
  }

  get formattedUpdatedAt() {
    return moment(this.updated_at).format('YYYY-MM-DD HH:mm:ss')
  }
}

ProductModel.init(
  {
    product_id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      field: 'PRODUCT_ID', 
    },
    product_name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'PRODUCT_NAME', 
    },
    sku_id: {
      type: DataTypes.UUID,
      allowNull: false,
      field: 'SKU_ID', 
      references: {
        model: SkuModel, // Tham chiếu đến model SpuModel
        key: 'sku_id', // Khóa chính của SpuModel
      },
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'DESCRIPTION', 
    },
    created_by: { // Sản phẩm được đăng bán bởi ai 
      type: DataTypes.UUID,
      allowNull: false,
      field: 'CREATED_BY', 
      references: {
        model: UserModel, // Tham chiếu đến model UserModel
        key: 'user_id', // Khóa chính của UserModel
      }
    },
    manufacturer: { // Nguồn gốc sản xuất 
      type: DataTypes.STRING,
      allowNull: false,
      field: 'MANUFACTURER', 
    },
    product_condition: { // Tình trạng mới hoặc cũ 
      type: DataTypes.STRING,
      allowNull: false,
      field: 'PRODUCT_CONDITION', 
    },
    product_status: { // tình trạng sản phẩm đã bán hãy chưa 
      type: DataTypes.STRING,
      allowNull: false,
      field: 'PRODUCT_STATUS', 
    },
    weight: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'WEIGHT', 
    },
    quantity: {
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'QUANTITY', 
    },
    product_price: { // GIÁ TIỀN KHI LÀ TÌNH TRẠNG CŨ  . Mỗi khách hàng sẽ có mức giá bán khác nhau. 
      type: DataTypes.FLOAT,
      allowNull: false,
      field: 'PRODUCT_PRICE', 
    },
    dimensions: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'DIMENSIONS', 
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: true,
      field: 'CREATED_AT', 
      get() {
        const rawValue = this.getDataValue('created_at');
        return moment(rawValue).format('YYYY-MM-DD HH:mm:ss');
      },
    } ,
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'UPDATED_AT', 
        get() {
          const rawValue = this.getDataValue('updated_at');
          return moment(rawValue).format('YYYY-MM-DD HH:mm:ss');
        },
    },
    deleted_at: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'DELETED_AT', 
    }
  },
  {
    sequelize,
    modelName: 'ProductModel', // Tên model 
    tableName: 'PRODUCT', // Tên bảng trong DB
    timestamps: false, // tắt quản lí timestamp
  }
);


export default ProductModel;
