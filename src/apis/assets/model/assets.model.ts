import moment from 'moment' 
import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import GenerateID from '../../../helpers/generateid'

class AssetModel extends Model {
  public asset_id!: string; //Thay đổi kiểu dữ liệu thành number
  public file_name!: string;//Tên file của asset
  public path!: string;//Đường dẫn tới file asset
  public type!: string;//Loại của asset (image, video, document,...)
  public size!: BigInt;//Kích thước của asset
  public module_name !: string;// Tên modules
  public module_id !: string;// id của module (product_id , banner id)
  public created_at!: Date;//Thời điểm tạo asset
  public updated_at!: Date;// Thời điểm cập nhật asset
  public deleted_at!: Date;// Thời điểm xóa asset (nếu có)
 
}

AssetModel.init(
  {
    asset_id: {
      type:DataTypes.STRING, // Thay đổi kiểu dữ liệu thành INTEGER
      defaultValue:GenerateID.generateWithCurrentTime.toString(),
      primaryKey: true,        // Thiết lập là khóa chính
      field: 'ASSET_ID', 
    },
    file_name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'FILE_NAME', 
    },
    path: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'PATH', 
    },
    type: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'TYPE', 
    },
    size: {
      type: DataTypes.BIGINT,
      allowNull: false,
      field: 'SIZE', 
    },
    module_name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'MODULE_NAME', 
    },
    module_id: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'MODULE_ID', 
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: true,
      field: 'CREATED_AT', 
      get() {
        const rawValue = this.getDataValue('created_at');
        return moment(rawValue).format('YYYY-MM-DD HH:mm:ss');
      },
    } ,
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'UPDATED_AT', 
        get() {
          const rawValue = this.getDataValue('updated_at');
          return moment(rawValue).format('YYYY-MM-DD HH:mm:ss');
        },
    },
    deleted_at: {
        type: DataTypes.DATE,
        allowNull: true,
        field: 'DELETED_AT', 
    }
  },
  {
    sequelize,
    modelName: 'AssetModel', // Tên model
    tableName: 'ASSET', // Tên bảng trong DB
    underscored: true, // Chỉ định các tên cột được tạo từ các tên mô hình như snake_case
    timestamps: false,
    hooks: {
      beforeUpdate:(instance, options) => {
        instance.setDataValue('updated_at', new Date());
      },
      // Hook 'beforeCreate' không cần thiết nữa do ID được tạo tự động
    },
  }
);

export default AssetModel;
