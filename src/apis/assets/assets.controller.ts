import  { Container } from 'typedi'
import { Response, Request } from 'express'
import AssetService from './service/assets.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'


const assetServiceInstance = Container.get(AssetService)

@Service()
class AssetController {

    async create  (req:Request, res:Response)  {
        try {
          const record = await assetServiceInstance.create(req.body)
          return new ResponseConfig(1000,record,'Asset was Create Successfully ').ResponseSuccess(req,res)

        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ AssetController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk  (req:Request, res:Response)  {
        try {
            const {data} = req.body
            const record = await assetServiceInstance.createBulk(data)
            return new ResponseConfig(1000,record,'Asset was Create Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AssetController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getByCondition  (req:Request, res:Response)  {
        try {
          const newUser = await assetServiceInstance.create(req.body)

          console.log("User created successfully")
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AssetController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }
    async getOne  (req:Request, res:Response)  {
        try {
          const newUser = await assetServiceInstance.create(req.body);
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AssetController ] : [getOne]' + err.message).ResponseSuccess(req,res)
        }
    }


    async uploadResource (req:Request, res:Response){
        try {
            const files = req.files as Express.Multer.File[];
            if (!files || files.length === 0) {
                return res.status(400).send('No files uploaded.');
            }
            const filePaths = files.map(file => file.path);
            res.status(200).send({ message: 'Files uploaded successfully.', filePaths: filePaths });
        } catch (err) {
            console.error(err);
            res.status(500).send('Internal server error.');
        }
    }

    async update  (req:Request, res:Response)  {
        try {
            const {asset_id,file_name,path,type,size,module_name,module_id} = req.body

            const options:any = {file_name,path,type,size,module_name,module_id}

            const record = await assetServiceInstance.update(asset_id,options);

            console.log("User created successfully")
            res.status(201).json({ message: 'User created successfully', data: record })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AssetController ] : [update]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request, res:Response)  {
        try {
            const {asset_id} = req.body
            const record = await assetServiceInstance.delete(asset_id)
            if(!record){
                return new ResponseConfig(2000,null,'Record not found in System ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,null,'Delete Record Successfully  ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ AssetController ] : [delete]' + err.message).ResponseSuccess(req,res)
        }
    }


    async getImage (req:Request, res:Response){
        try{

        }catch(error:any){


        }
    }

    async getVideo (req:Request, res:Response){
        try{

        }catch(error:any){


        }
    }


    
}

export default new AssetController()



