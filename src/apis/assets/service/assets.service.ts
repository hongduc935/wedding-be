import  { Service } from 'typedi'
import  {AssetModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class AssetService {
  async create(record:any) {
    return await AssetModel.create(record)
  }

  async createBulk(records:any) {
    return await AssetModel.bulkCreate(records)
  }

  async getById(id: string) {
    return await AssetModel.findByPk(id)
  }

  async update(id: number, data: any) {
    const record = await AssetModel.findByPk(id)
    if (record) {
      return await record.update(data)
    }
    return null
  }

  async delete(id: string) {
    const record = await AssetModel.findByPk(id)
    if (record) {
       await record.destroy()
       return true 
    }
    return null 
  }

  async findUserByAndCondition(condition: { [key: string]: any }) {
    return await AssetModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findUserByOrCondition(condition: { [key: string]: any }) {
    return await AssetModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default AssetService;
