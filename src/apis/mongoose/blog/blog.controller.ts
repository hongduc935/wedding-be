import { Container }from 'typedi'
import { Response, Request } from 'express';
import BlogService from './service/blog.service'
import { Service } from 'typedi';
import ResponseConfig from '../../../config/response-http'
@Service()
class BlogController {

    async create(req: Request, res: Response) {
        try {
            const data: any = req.body;
    
            // Chạy các promise song song để kiểm tra tồn tại của slug và domain cùng lúc
            const [checkExistedSlug, checkDomain] = await Promise.all([
                BlogService.findBlogSlug(data.slug),
                BlogService.findBlogByDomain(data.domain)
            ]);
    
            if (checkExistedSlug && checkDomain) {
                return new ResponseConfig(2000, null, `Slug existed in Domain ${data.domain}, please choose another slug.`)
                    .ResponseSuccess(req, res);
            }
    
            // Tạo blog sau khi các điều kiện được kiểm tra
            const record = await BlogService.create(data);
            return new ResponseConfig(1000, record, 'Blog was created successfully.')
                .ResponseSuccess(req, res);
    
        } catch (error: any) {
            return new ResponseConfig(2000, null, 'Error in [ BlogController ]: [create] - ' + error.message)
                .ResponseSuccess(req, res);
        }
    }
    

    async getBlogMac(req:Request, res:Response) {
        try {
            const {category,page,limit,domain} = req.body
            const record = await BlogService.findBlog(category,domain,page,limit)
            return new ResponseConfig(1000,record,'Blog Get All Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ BlogController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }


    
    async getBlogBySlug(req:Request, res:Response) {
        try {
            const {slug} = req.params
            const record = await BlogService.findBlogSlug(slug)
            return new ResponseConfig(1000,record,'Blog Blog By Slug Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ BlogController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }


    async updateContentBlog(req:Request, res:Response) {
        try {
            const {id}= req.params
            const data = req.body
            const record = await BlogService.updateContentBlog(id,data)
            return new ResponseConfig(1000,record,'Blog Blog By Slug Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ BlogController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }


}

export default new BlogController()



