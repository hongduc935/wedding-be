import mongoose from "mongoose";

const BlogSchema = new mongoose.Schema(
  {
    blog_title: {
      // Dùng cho thẻ title và cả thẻ H1
      type: String,
      required: true,
    },
    blog_meta_description: {
      // Dùng cho meta description
      type: String,
      required: true,
    },
    blog_keywords: {
      // Từ khóa SEO cho bài viết
      type: String,
      required: false,
    },
    blog_short_content: {
      // Mô tả ngắn bổ trợ cho tiêu đề thẻ H1
      type: String,
      required: true,
    },
    blog_main_image: {
      // Ảnh chính cho bài viết nếu có
      type: String,
      required: true,
    },
    blog_og_image: {
      // Ảnh Open Graph khi chia sẻ trên mạng xã hội
      type: String,
      required: false,
    },
    blog_content: {
      // Nội dung HTML của bài viết
      type: String,
      required: true,
    },
    category: {
      // Danh mục bài viết
      type: String, // Ví dụ: Mac, AirPods, iPhone, ...
      required: true,
    },
    domain: {
      // Domain sử dụng blog này
      type: String,
      required: true, 
    },
    slug: {
      // Đường dẫn cho blog
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const BlogModels = mongoose.model("blogs", BlogSchema);
export default BlogModels;
