

import  { Service } from 'typedi'
import BlogModels from '../model/blog.model'
import mongoose from 'mongoose'
@Service()
class BlogService {

  async create(data:any) {
    return await BlogModels.create(data)
  }

  async findBlog(category: string,domain:string, page: number, limit: number) {
    let query:any = {};
    if (category) {
      query.category = category;
    }
    const skip = (page - 1) * limit;

    return await BlogModels.find(query).skip(skip).limit(limit);
  }
  

  async findBlogSlug(slug:string) {
    return await BlogModels.findOne({slug})
  }

  async findBlogByDomain(domain:string) {
    return await BlogModels.findOne({domain})
  }

  async updateContentBlog(id:string,data:any) {
    return await BlogModels.findByIdAndUpdate({_id:new mongoose.Types.ObjectId(id)},data)
  }

}

export default new BlogService();
