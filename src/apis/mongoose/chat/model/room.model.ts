import mongoose from 'mongoose';

const RoomSchema = new mongoose.Schema({
  roomId: {
    type: String,
    required: true,
    unique: true,
  },
  users: [{
    type: String, // ID của người dùng
    required: true,
  }],
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const RoomModels = mongoose.model('rooms', RoomSchema);

export default RoomModels;
