import mongoose from 'mongoose';

const messageSchema = new mongoose.Schema({
  sender: {
    type: String,
    required: true,
  },
  receiver: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  file_url:[{
    type: String,
    required: false,
  }],
  timestamp: {
    type: Date,
    default: Date.now,
  },
});

const MessagesModel = mongoose.model('messages', messageSchema);

export default MessagesModel;
