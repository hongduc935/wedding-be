import MessagesModel from '../model/message.model'
import  { Service } from 'typedi'
import mongoose from 'mongoose'

@Service()
class ChatService {

  async create(data:any) {
    const message = new MessagesModel(data);
    return await message.save();
  }

  async findPrivateMessage({userId,friendId}) {
    return await MessagesModel.find({
        $or: [
          { sender: userId, receiver: friendId },
          { sender: friendId, receiver: userId },
        ],
      }).sort({ timestamp: 1 });
  }

}

export default new ChatService();

