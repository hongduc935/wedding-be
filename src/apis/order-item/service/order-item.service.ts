import  { Service } from 'typedi'
import  {OrderItemModel} from '../../index'

@Service()
class OrderItemService {

  async create(data:any) {
    return await OrderItemModel.create(data);
  }

  async getById(id: number) {
    return await OrderItemModel.findByPk(id);
  }

  async getAll() {
    return await OrderItemModel.findAndCountAll();
  }

  async bulkCreate(records:any[]) {
    return await OrderItemModel.bulkCreate(records);
  }

  async update(id: number, data: any) {
    const record = await OrderItemModel.findByPk(id);
    if (record) {
      return await record.update(data)
    }
    return null 
  }
}

export default OrderItemService;
