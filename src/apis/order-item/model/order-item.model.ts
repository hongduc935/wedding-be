
import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import moment from 'moment' 
import { v4 as uuidv4 } from 'uuid'
import OrderModel from '../../order/model/order.model'
import ProductModel from '../../product/model/product.model'


class OrderItemModel extends Model {
    public order_item_id!: string;
    public order_id!: string;
    public product_id!: string;
    public quantity!: number;
    public price!: number;
    public created_at!: Date;
    public updated_at!: Date;
    public deleted_at!: Date | null;
}
  
OrderItemModel.init({
        order_item_id: {
            type: DataTypes.UUID,
            defaultValue: () => uuidv4(),
            primaryKey: true,
            field: 'ORDER_ITEM_ID', 
        },
        order_id: {
            type:DataTypes.UUID,
            allowNull: false,
            field: 'ORDER_ID',
            references: {
                model: OrderModel,
                key: 'order_id'
            },
        },
        product_id: {
            type: DataTypes.UUID,
            allowNull: false,
            field: 'PRODUCT_ID',
            references: {
                model: ProductModel,
                key: 'product_id',
              }
        },
        quantity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'QUANTITY',
        },
        price: {
            type: DataTypes.FLOAT,
            allowNull: false,
            field: 'PRICE',
        },
        created_at: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW,
            field: 'CREATED_AT',
            get() {
                const rawValue = this.getDataValue('created_at')
                return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
            },
        },
        updated_at: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW,
            field: 'UPDATED_AT',
            get() {
                const rawValue = this.getDataValue('updated_at')
                return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
            },
        },
        deleted_at: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'DELETED_AT',
            get() {
                const rawValue = this.getDataValue('deleted_at')
                return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
            },
        },
    }, 
    {
        sequelize,
        tableName: 'ORDER_ITEM', // Tên bảng trong DB
        modelName: 'OrderItemModel',
        timestamps: false,
        paranoid: true,
    }
);

export default OrderItemModel
