import { Container } from 'typedi'
import { Response, Request } from 'express'
import OrderItemService from './service/order-item.service'
import { Service } from 'typedi';
import ResponseConfig from '../../config/response-http'
const orderItemServiceInstance = Container.get(OrderItemService)

@Service()
class OrderItemController {

    async create(req:Request, res:Response) {
        try {
            const data = req.body
            const record:any  = await orderItemServiceInstance.create(data)
            return new ResponseConfig(1000,record,'Category was Create Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ OrderItemController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }

    async bulkCreate(req:Request, res:Response) {
        try {
            const {data} = req.body
            const records = await orderItemServiceInstance.bulkCreate(data)
            return new ResponseConfig(1000,records,'Category create many record successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ OrderItemController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }


    async getById (req:Request, res:Response) {
        try {
            const {id} = req.body
            const record = await orderItemServiceInstance.getById(Number(id))
            return new ResponseConfig(1000,record,'Category was Create Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderItemController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }
    async getAll (req:Request, res:Response) {
        try {
            const record = await orderItemServiceInstance.getAll()
            return new ResponseConfig(1000,record,'Category was Create Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderItemController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }
    
    async update  (req:Request, res:Response) {
        try {
            const {category_id,category_name} = req.body

            const options = {category_name}

            const record = await orderItemServiceInstance.update(Number(category_id),options)

            if(!record){
                return new ResponseConfig(2000,null,'Record not found in System ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,record,'Update category success ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderItemController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

}

export default new OrderItemController()



