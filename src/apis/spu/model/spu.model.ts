import { Model, DataTypes} from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import CategoryModel from '../../category/model/category.model'
import ProductModel from '../../product/model/product.model'
import SkuModel from '../../sku/model/sku.model'
import { v4 as uuidv4 } from 'uuid'

class SpuModel extends Model {
    public spu_id!: string;
    public spu_code!: string;
    public product_name!: string; // tên sản phẩm 
    public description!: string; // mô tả chung về loại sản phẩm 
    public category_id!: string; // danh mục sản phẩm 
    public created_at!: string;
    public updated_at!: string;
    public deleted_at!: string;
}

SpuModel.init({
    spu_id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        primaryKey: true,
        field: 'SPU_ID'
    },
    spu_code: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'SPU_CODE'
    },
    product_name: {
        type: DataTypes.TEXT,
        allowNull: true,
        field: 'PRODUCT_NAME'
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true,
        field: 'DESCRIPTION'
    },
    category_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        references: {
            model: CategoryModel,
            key: 'category_id'
        },
        field: 'CATEGORY_ID'
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'CREATED_AT'
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'UPDATED_AT'
    },
    deleted_at:{
        type: DataTypes.DATE,
        defaultValue: null,
        field: 'DELETED_AT'
    },
}, {
    sequelize,
    modelName: 'spu',
    tableName: 'SPU',
    underscored: true,
    timestamps: false,
});

export default SpuModel
