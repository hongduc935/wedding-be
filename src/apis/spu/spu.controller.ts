import  { Container } from 'typedi'
import { Response, Request } from 'express'
import SpuService from './service/spu.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'


const spuServiceInstance = Container.get(SpuService)

@Service()
class SpuController {

    async create (req:Request, res:Response)  {
        try { 
            const record = await spuServiceInstance.create(req.body)
            return new ResponseConfig(1000,record,'SPU create successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SpuController ] : [ create ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk (req:Request, res:Response)  {
        try { 

            const {data} = req.body
            const records = await spuServiceInstance.createBulk(data)
            return new ResponseConfig(1000,records,'SPU create bulk successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SpuController ] : [ createBulk ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAll (req:Request, res:Response)  {
        try { 
            const records = await spuServiceInstance.findAll()
            return new ResponseConfig(1000,records,'SPU Find All  successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SpuController ] : [ createBulk ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getOne  (req:Request, res:Response)  {
        try {
          const newUser = await spuServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
           return new ResponseConfig(2000,null,'Error system in [ SpuController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update  (req:Request, res:Response)  {
        try {
            const newUser = await spuServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
             return new ResponseConfig(2000,null,'Error system in [ SpuController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request, res:Response)  {
        try {
            const newUser = await spuServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SpuController ] : [ delete ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new SpuController()



