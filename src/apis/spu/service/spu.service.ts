import  { Service } from 'typedi'
import  {SpuModel} from '../../index'
import { Op } from 'sequelize'



@Service()
class SpuService {

  async create(data:any) {
    return await SpuModel.create(data);
  }

  async findAll() {
    return await SpuModel.findAndCountAll();
  }

  async createBulk(data:any[]) {
    return await SpuModel.bulkCreate(data);
  }

  async getById(id: number) {
    return await SpuModel.findByPk(id);
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await SpuModel.findByPk(id);
    if (user) {
      return await user.update(data);
    }
    throw new Error('User not found');
  }

  async delete(id: number) {
    const user = await SpuModel.findByPk(id);
    if (user) {
      return await user.destroy();
    }
    throw new Error('User not found');
  }

  async findUserByAndCondition(condition: { [key: string]: any }) {
    return await SpuModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findUserByOrCondition(condition: { [key: string]: any }) {
    return await SpuModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default SpuService;
