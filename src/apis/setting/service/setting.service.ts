import  { Service } from 'typedi'
import  {SettingModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class SettingService {
  async create(data:any) {
    return await SettingModel.create(data)
  }

  async getById(id: number) {
    return await SettingModel.findByPk(id)
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await SettingModel.findByPk(id);
    if (user) {
      return await user.update(data);
    }
    throw new Error('User not found');
  }

  async delete(id: number) {
    const user = await SettingModel.findByPk(id);
    if (user) {
      return await user.destroy();
    }
    throw new Error('User not found');
  }

  async findUserByAndCondition(condition: { [key: string]: any }) {
    return await SettingModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findUserByOrCondition(condition: { [key: string]: any }) {
    return await SettingModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default SettingService
