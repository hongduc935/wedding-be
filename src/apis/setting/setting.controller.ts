import { Container } from 'typedi'
import { Response, Request } from 'express'
import SettingService from './service/setting.service'
import ResponseConfig from '../../config/response-http'

const settingServiceInstance = Container.get(SettingService)

const SettingController = {

    getByCondition : async (req:Request, res:Response) => {
        try {
          const newUser = await settingServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ SettingController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    },
    getOne : async (req:Request, res:Response) => {
        try {
          const newUser = await settingServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ SettingController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    },
    update : async (req:Request, res:Response) => {
        try {
            const newUser = await settingServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ SettingController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    },

}

export default SettingController



