import  { Service } from 'typedi'
import  {RoleModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class RoleService {

  async create(data:any) {
    return await RoleModel.create(data);
  }

  async createBulk(data:any) {
    return await RoleModel.bulkCreate(data);
  }

  async findAll() {
    return await RoleModel.findAndCountAll()
  }

  async getById(id: number) {
    return await RoleModel.findByPk(id);
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await RoleModel.findByPk(id);
    if (user) {
      return await user.update(data);
    }
    throw new Error('User not found');
  }

  async delete(id: number) {
    const user = await RoleModel.findByPk(id);
    if (user) {
      return await user.destroy();
    }
    throw new Error('User not found');
  }

  async findUserByAndCondition(condition: { [key: string]: any }) {
    return await RoleModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findUserByOrCondition(condition: { [key: string]: any }) {
    return await RoleModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default RoleService;
