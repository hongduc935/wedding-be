import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'

class RoleModel extends Model {
  public role_id!: number;  // Thay đổi kiểu dữ liệu thành number
  public role_name!: string;
}

RoleModel.init(
  {
    role_id: {
      type: DataTypes.INTEGER, // Thay đổi kiểu dữ liệu thành INTEGER
      autoIncrement: true,     // Thiết lập autoIncrement
      primaryKey: true,        // Thiết lập là khóa chính
      field: 'ROLE_ID', 
    },
    role_name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'ROLE_NAME', 
    }
  },
  {
    sequelize,
    modelName: 'RoleModel', // Tên model
    tableName: 'ROLE', // Tên bảng trong DB
    timestamps: false,
    hooks: {
      // Hook 'beforeCreate' không cần thiết nữa do ID được tạo tự động
    },
  }
);

export default RoleModel;
