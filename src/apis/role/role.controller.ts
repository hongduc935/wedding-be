import { Container }from 'typedi'
import { Response, Request } from 'express'
import RoleService from './service/role.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'


const roleServiceInstance = Container.get(RoleService)

@Service()
class RoleController {

    async create (req:Request, res:Response)  {
        try {
          const newUser = await roleServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ RoleController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk (req:Request, res:Response)  {
        try {

            const {data} = req.body
            const records = await roleServiceInstance.createBulk(data)
            return new ResponseConfig(1000,records,'Create Bulk Role Success').ResponseSuccess(req,res)

        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ RoleController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAll (req:Request, res:Response)  {
        try {
            const records = await roleServiceInstance.findAll()
            return new ResponseConfig(1000,records,'Find All Role Success').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ RoleController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getOne (req:Request, res:Response)  {
        try {
          const newUser = await roleServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ RoleController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update (req:Request, res:Response)  {
        try {
            const newUser = await roleServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ RoleController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete (req:Request, res:Response)  {
        try {
            const newUser = await roleServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ RoleController ] : [ delete ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new RoleController()



