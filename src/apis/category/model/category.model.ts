import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import moment from 'moment' 

class CategoryModel extends Model {
  public category_id!: number  // Thay đổi kiểu dữ liệu thành number
  public category_name!: string
  created_at!:Date
  updated_at!:Date
}

CategoryModel.init(
  {
    category_id: {
      type: DataTypes.INTEGER, // Thay đổi kiểu dữ liệu thành INTEGER
      autoIncrement: true,     // Thiết lập autoIncrement
      primaryKey: true,        // Thiết lập là khóa chính
      field: 'CATEGORY_ID', 
    },
    category_name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'CATEGORY_NAME', 
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: true,
      field: 'CREATED_AT', 
      get() {
        const rawValue = this.getDataValue('created_at')
        return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
      },
    } ,
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'UPDATED_AT', 
        get() {
          const rawValue = this.getDataValue('updated_at')
          return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
        },
    }
  },
  {
    sequelize,
    modelName: 'category', // Tên model
    tableName: 'CATEGORY', // Tên bảng trong DB
    timestamps: false, // Tắt tự động quản lý timestamps
    // underscored: true, // Chỉ định các tên cột được tạo từ các tên mô hình như snake_case
    hooks: {
      beforeUpdate:(instance, options) => {
        instance.setDataValue('updated_at', new Date())
      },
    },
  },
  
)

export default CategoryModel
