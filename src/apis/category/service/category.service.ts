import  { Service } from 'typedi'
import  {CategoryModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class CategoryService {

  async create(data:any) {
    return await CategoryModel.create(data);
  }

  async getById(id: number) {
    return await CategoryModel.findByPk(id);
  }

  async getAll() {
    return await CategoryModel.findAndCountAll();
  }

  async bulkCreate(records:any[]) {
    return await CategoryModel.bulkCreate(records);
  }

  async update(id: number, data: any) {
    const record = await CategoryModel.findByPk(id);
    if (record) {
      return await record.update(data)
    }
    return null 
  }
}

export default CategoryService;
