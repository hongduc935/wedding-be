import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import { v4 as uuidv4 } from 'uuid'
import ProductModel from '../../product/model/product.model'
import AssetModel from '../../assets/model/assets.model'



class ProductAssetModel extends Model {
  public product_asset_id!: string; //	Primary key của bảng
  public product_id!: string; //	Khóa ngoại đến bảng Product
  public asset_id!: string; // 	Khóa ngoại đến bảng Asset
  public type!: string;//	Loại của asset (ví dụ: ảnh chính, ảnh phụ, ...)
}

ProductAssetModel.init(
  {
    product_asset_id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      field: 'PRODUCT_ASSET_ID', 
    },
    product_id: {
      type: DataTypes.UUID,
      allowNull: false,
      field: 'PRODUCT_ID', 
      references: {
        model: ProductModel,
        key: 'product_id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    asset_id: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'ASSET_ID', 
        references: {
          model: AssetModel,
          key: 'asset_id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
    },
    type: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'TYPE', 
    }
  },
  {
    sequelize,
    modelName: 'ProductAssetModel', // Tên model
    tableName: 'PRODUCT_ASSET', // Tên bảng trong DB
    timestamps: false
  }
);

export default ProductAssetModel;
