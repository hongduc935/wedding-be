import  { Service } from 'typedi'
import  {ProductAssetModel} from '../../index'
import { Op } from 'sequelize'


@Service()
class ProductAssetService {

  async create(data:any) {
    return await ProductAssetModel.create(data)
  }

  async createBulk(data:any[]) {
    return await ProductAssetModel.bulkCreate(data)
  }

  async getById(id: number) {
    return await ProductAssetModel.findByPk(id)
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await ProductAssetModel.findByPk(id)
    if (user) {
      return await user.update(data)
    }
    throw new Error('Item not found')
  }

  async delete(id: number) {
    const user = await ProductAssetModel.findByPk(id)
    if (user) {
      return await user.destroy()
    }
    throw new Error('Item not found')
  }
}

export default ProductAssetService;
