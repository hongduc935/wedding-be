import  { Container } from 'typedi'
import { Response, Request } from 'express'
import ProductAssetService from './service/product-asset.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'


const productAssetServiceInstance = Container.get(ProductAssetService)

@Service()
class ProductAssetController {

    async create (req:Request, res:Response)  {
        try {
          const record  = await productAssetServiceInstance.create(req.body)
          return new ResponseConfig(1000,record,'Product Image was Create Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk (req:Request, res:Response)  {
        try {
            const { data } = req.body 
            const record  = await productAssetServiceInstance.createBulk(data)
            return new ResponseConfig(1000,record,'Product Image was Bulk Create Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async update (req:Request, res:Response)  {
        try {
            const body = req.body
            const {product_asset_id}  = body
            delete body.product_asset_id;
            const newUser = await productAssetServiceInstance.update(product_asset_id,body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete (req:Request, res:Response)  {
        try {
            const newUser = await productAssetServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new ProductAssetController()



