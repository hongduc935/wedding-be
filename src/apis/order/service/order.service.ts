import  { Service } from 'typedi'
import  {OrderModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class OrderService {

  async create(data:any) {
    return await OrderModel.create(data)
  }

  async getById(id: number) {
    return await OrderModel.findByPk(id)
  }

  async getAll() {
    return await OrderModel.findAndCountAll()
  }

  async bulkCreate(records:any[]) {
    return await OrderModel.bulkCreate(records)
  }

  async update(id: number, data: any) {
    const record = await OrderModel.findByPk(id)
    if (record) {
      return await record.update(data)
    }
    return null 
  }

  async findAndCondition(condition: { [key: string]: any }) {
    return await OrderModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findOrCondition(condition: { [key: string]: any }) {
    return await OrderModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default OrderService;
