import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import moment from 'moment' 
import { v4 as uuidv4 } from 'uuid'

class OrderModel extends Model {
    public order_id!: string;
    public user_id!: string;
    public order_date!: Date;//Ngày đặt hàng
    public total_amount!: number;//Tổng số tiền của đơn hàng.
    public status!: string;//Trạng thái đơn hàng (Pending, Completed, Cancelled, v.v.)
    public shipping_address!: string;//Địa chỉ giao hàng.
    public payment_method!: string;//Phương thức thanh toán (Credit Card, PayPal, Cash on Delivery, v.v.).
    public created_at!: Date;//Thời điểm tạo đơn hàng.
    public updated_at!: Date;//Thời điểm cập nhật đơn hàng.
}

OrderModel.init(
  {
    order_id:  {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        primaryKey: true,
        field: 'ORDER_ID', 
    },
    user_id: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'USER_ID',
    },
    order_date: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
        field: 'ORDER_DATE',
    },
    total_amount: {
        type: DataTypes.FLOAT,
        allowNull: false,
        field: 'TOTAL_AMOUNT',
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'STATUS',
    },
    shipping_address: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'SHIPPING_ADDRESS',
    },
    payment_method: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'PAYMENT_METHOD',
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: true,
      field: 'CREATED_AT', 
      get() {
        const rawValue = this.getDataValue('created_at')
        return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
      },
    } ,
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'UPDATED_AT', 
        get() {
          const rawValue = this.getDataValue('updated_at')
          return moment(rawValue).format('YYYY-MM-DD HH:mm:ss')
        },
    }
  },
  {
    sequelize,
    modelName: 'OrderModel', // Tên model
    tableName: 'ORDER', // Tên bảng trong DB
    timestamps: false, // Tắt tự động quản lý timestamps
    // underscored: true, // Chỉ định các tên cột được tạo từ các tên mô hình như snake_case
    hooks: {
      beforeUpdate:(instance, options) => {
        instance.setDataValue('updated_at', new Date())
      },
    },
  },
  
)

export default OrderModel
