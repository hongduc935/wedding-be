import { Container } from 'typedi'
import { Response, Request } from 'express'
import OrderService from './service/order.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'

const orderServiceInstance = Container.get(OrderService)

@Service()
class OrderController {

    async create(req:Request, res:Response) {
        try {
            // Tạo đơn hàng 
            const result:any  = await orderServiceInstance.create(req.body)
            //Tạo order Item
            return new ResponseConfig(1000,result,'Order was Create Successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ OrderController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }

    async bulkCreate(req:Request, res:Response) {
        try {
            await orderServiceInstance.bulkCreate(req.body)
            return new ResponseConfig(1000,{},'Order buld create many record successfully ').ResponseSuccess(req,res)
        } catch (error:any) {
            return new ResponseConfig(2000,null,'Error system in [ OrderController ] : [create]' + error.message).ResponseSuccess(req,res)
        }
    }


    async getById (req:Request, res:Response) {
        try {
            const {id} = req.body
            const record = await orderServiceInstance.getById(Number(id))
            return new ResponseConfig(1000,record,'Get Order By Id Success').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAndCondition (req:Request, res:Response) {
        try {
            const {user_id} = req.body
            const options = {user_id}
            const record = await orderServiceInstance.findAndCondition(options)
            return new ResponseConfig(1000,record,'Find Order By User ID Success').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getAll (req:Request, res:Response) {
        try {
            const record = await orderServiceInstance.getAll()
            return new ResponseConfig(1000,record,'Order find all  Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }
    
    async update  (req:Request, res:Response) {
        try {
            const {category_id,category_name} = req.body

            const options = {category_name}

            const record = await orderServiceInstance.update(Number(category_id),options)

            if(!record){
                return new ResponseConfig(2000,null,'Record not found in System ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,record,'Update Order success ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ OrderController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new OrderController()



