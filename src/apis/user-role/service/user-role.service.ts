import  { Service } from 'typedi'
import  {UserRoleModel} from '../../index'
import { Op } from 'sequelize'



@Service()
class UserRoleService {
    
  async create(data:any) {
    return await UserRoleModel.create(data)
  }

  async findOrCondition(condition: { [key: string]: any }) {
    return await UserRoleModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }


  async update(id: number, data: { username?: string; email?: string }) {
    const user = await UserRoleModel.findByPk(id)
    if (user) {
      return await user.update(data)
    }
    return null
  }

  async delete(id: number) {
    const user = await UserRoleModel.findByPk(id)
    if (user) {
       await user.destroy()
       return true 
    }
    return null 
  }

}

export default UserRoleService
