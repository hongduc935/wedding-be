import { Container } from 'typedi'
import { Response, Request } from 'express'
import UserRoleService from './service/user-role.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'
const userRoleServiceInstance = Container.get(UserRoleService)

@Service()
class UserRoleController {

    async create  (req:Request, res:Response) {
        try {
          const record  = await userRoleServiceInstance.create(req.body)
          return new ResponseConfig(1000,record,'Created Permission successfully').ResponseSuccess(req,res)
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ UserRoleController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async update  (req:Request, res:Response) {
        try {
            const {id} = req.body
            const record = await userRoleServiceInstance.update(id,req.body)
            res.status(201).json({ message: 'User created successfully', data: record });
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ UserRoleController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request , res:Response)  {
        try {
            const result:any = await userRoleServiceInstance.delete(req.body)
            if(!result){
                return new ResponseConfig(2000,null,'Record not found. ').ResponseSuccess(req,res)
            }
            return new ResponseConfig(1000,result,'Delete Success  ').ResponseSuccess(req,res)
        } catch (err:any) {
            return new ResponseConfig(2000,null,'Error system in [ UserRoleController ] : [ delete ]' + err.message).ResponseSuccess(req,res) 
        }
    }
}

export default new UserRoleController()



