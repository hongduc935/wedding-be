import { Model, DataTypes} from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import UserModel from '../../auth/model/auth.model';
import RoleModel from '../../role/model/role.model';
// import CategoryModel from '../../category/model/category.model';


// const Category = require('./Category');
// const Brand = require('./Brand');

class UserRoleModel extends Model {
    public user_role_id!: string;
    public user_id!: string;
    public role_id!: string;
}

UserRoleModel.init({
    user_role_id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        field: 'USER_ROLE_ID', 
    },
    user_id: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'USER_ID', 
        references: {
            model: UserModel,
            key: 'user_id'
        },
    },
    role_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        field: 'ROLE_ID',
        references: {
            model: RoleModel,
            key: 'role_id'
        },
    },
}, {
    sequelize,
    modelName: 'UserRoleModel',
    tableName: 'USER_ROLE',
    underscored: true,
    timestamps: false,
})

export default UserRoleModel
