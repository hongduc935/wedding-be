import { Model, DataTypes } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import AttributeModel from '../../attribute/model/attribute.model'
import ProductModel from '../../product/model/product.model'
import { v4 as uuidv4 } from 'uuid'

class ProductAttributeModel extends Model {
    public product_attribute_id!: string // khóa chính 
    public product_id!: string // id của product 
    public attribute_id !: string // thuộc tính id 
    public attribute_value !: string // giá trị của thuộc tính ví dụ thuộc tính màu sắc có giá trị là xanh 
    public created_at !: Date
    public updated_at !: Date
}

ProductAttributeModel.init({
    product_attribute_id: {
        type: DataTypes.UUID,
        defaultValue: () => uuidv4(),
        primaryKey: true,
        field: 'PRODUCT_ATTRIBUTE_ID'
    },
    product_id: {
        type: DataTypes.UUID,
        allowNull: false,
        field: 'PRODUCT_ID',
        references: {
            model: ProductModel,
            key: 'product_id'
        }, 
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    },
    attribute_id:{
        type: DataTypes.UUID, 
        allowNull: false,
        field: 'ATTRIBUTE_ID',
        references: {
            model: AttributeModel,
            key: 'attribute_id'
        }, 
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
    },
    attribute_value:{
        type: DataTypes.STRING,
        allowNull: false,
        field: 'ATTRIBUTE_VALUE'
    },
    created_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'CREATED_AT'
    },
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        field: 'UPDATED_AT'
    }
}, {
    sequelize,
    modelName: 'ProductAttributeModel',
    tableName: 'PRODUCT_ATTRIBUTE',
    timestamps: false,
    underscored: true
});

export default ProductAttributeModel;
