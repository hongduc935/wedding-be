import  { Service } from 'typedi'
import  {ProductAttributeModel} from '../../index'
import { Op } from 'sequelize'


@Service()
class ProductAttributeService {

  async create(data:any) {
    return await ProductAttributeModel.create(data)
  }

  async createBulk(data:any[]) {
    return await ProductAttributeModel.bulkCreate(data)
  }

  async findAll(attributeIds) {
    return await ProductAttributeModel.findAndCountAll({
        where: {
            attribute_id: attributeIds
        },
        attributes: ['product_id'],
        group: 'product_id'
    })
  }

  async getById(id: number) {
    return await ProductAttributeModel.findByPk(id)
  }

  async update(id: number, data: { username?: string; email?: string }) {
    const user = await ProductAttributeModel.findByPk(id)
    if (user) {
      return await user.update(data);
    }
    return null 
  }

  async delete(id: number) {
    const user = await ProductAttributeModel.findByPk(id)
    if (user) {
      return await user.destroy();
    }
    return null 
  }

  async findUserByAndCondition(condition: { [key: string]: any }) {
    return await ProductAttributeModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findUserByOrCondition(condition: { [key: string]: any }) {
    return await ProductAttributeModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default ProductAttributeService;
