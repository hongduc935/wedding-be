import  { Container } from 'typedi'
import { Response, Request } from 'express'
import ProductAttributeService from './service/product-attribute.service'
import { Service } from 'typedi'
import ResponseConfig from '../../config/response-http'


const productAttributeServiceInstance = Container.get(ProductAttributeService)

@Service()
class ProductAttributeController {

    async create  (req:Request, res:Response)  {
        try {
          const newUser = await productAttributeServiceInstance.create(req.body)

          console.log("User created successfully")
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductAttributeController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk  (req:Request, res:Response)  {
        try {
            const {data} = req.body
            const record = await productAttributeServiceInstance.createBulk(data)
            return new ResponseConfig(1000,record,'ProductAttribute was Bulk Create Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductAttributeController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async findAll  (req:Request, res:Response)  {
        try {
            const record = await productAttributeServiceInstance.findAll({})
            return new ResponseConfig(1000,null,'Find All ProductAttribute Successfully ').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductAttributeController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }

    async update  (req:Request, res:Response)  {
        try {
            const newUser = await productAttributeServiceInstance.create(req.body);

            console.log("User created successfully")
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductAttributeController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request, res:Response)  {
        try {
            const newUser = await productAttributeServiceInstance.create(req.body);
            console.log("User created successfully")
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ ProductAttributeController ] : [ delete ]' + err.message).ResponseSuccess(req,res)
        }
    }
}

export default new ProductAttributeController()



