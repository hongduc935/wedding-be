import  { Service } from 'typedi';
import  {SeoModel} from '../../index'
import { Op } from 'sequelize';
@Service()
class SeoService {
  async create(data:any) {
    return await SeoModel.create(data);
  }

  async getById(id: number) {
    return await SeoModel.findByPk(id);
  }

  async update(id: number, data: { page_title?: string; page_des?: string }) {
    const user = await SeoModel.findByPk(id);
    if (user) {
      return await user.update(data);
    }
    throw new Error('User not found');
  }

  async delete(id: number) {
    const user = await SeoModel.findByPk(id);
    if (user) {
      return await user.destroy();
    }
    throw new Error('User not found');
  }

  async findByAndCondition(condition: { [key: string]: any }) {
    return await SeoModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findByOrCondition(condition: { [key: string]: any }) {
    return await SeoModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default SeoService;
