import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'

class SeoModel extends Model {
  public seo_id!: number // Thay đổi kiểu dữ liệu thành number
  public page_id!: string
  public page_title!: string
  public page_des!: string
}

SeoModel.init(
  {
    seo_id: {
      type: DataTypes.INTEGER, // Thay đổi kiểu dữ liệu thành INTEGER
      autoIncrement: true,     // Thiết lập autoIncrement
      primaryKey: true,        // Thiết lập là khóa chính
      field: 'SEO_ID', 
    },
    page_id: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'PAGE_ID', 
    },
    page_title: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'PAGE_TITLE', 
    },
    page_des: {
        type: DataTypes.STRING,
        allowNull: false,
        field: 'PAGE_DES', 
    },
    canonical_url:{
        type: DataTypes.STRING,
        allowNull: false,
        field: 'CANONOCAL_URL', 
    }
  },
  {
    sequelize,
    modelName: 'SeoModel', // Tên model
    tableName: 'SEO', // Tên bảng trong DB
    timestamps:false,
    hooks: {
      // Hook 'beforeCreate' không cần thiết nữa do ID được tạo tự động
    },
  }
);

export default SeoModel;
