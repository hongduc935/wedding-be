import { Container } from 'typedi'
import { Response, Request } from 'express'
import SeoService from './service/seo.service'
import ResponseConfig from '../../config/response-http'

const seoServiceInstance = Container.get(SeoService)

const SeoController = {

    getByCondition : async (req:Request, res:Response) => {
        try {
          const newUser = await seoServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
         return new ResponseConfig(2000,null,'Error system in [ SeoController ] : [ getByCondition ]' + err.message).ResponseSuccess(req,res)
        }
    },
    getOne : async (req:Request, res:Response) => {
        try {
          const newUser = await seoServiceInstance.create(req.body)
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
         return new ResponseConfig(2000,null,'Error system in [ SeoController ] : [ getOne ]' + err.message).ResponseSuccess(req,res)
        }
    },
    update : async (req:Request, res:Response) => {
        try {
            const newUser = await seoServiceInstance.create(req.body)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
           return new ResponseConfig(2000,null,'Error system in [ SeoController ] : [ update ]' + err.message).ResponseSuccess(req,res)
        }
    },
}

export default SeoController



