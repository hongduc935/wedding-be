import AuthUtils from '../../utils/Auth'
import UserService from './service/auth.service'
import { Container } from 'typedi'
import { Request, Response } from 'express'
import { Service } from 'typedi'
import CryptoJS from 'crypto-js'
import wrapWithLogging from '../../config/logger-decorator'
import ResponseConfig from '../../config/response-http'
import UserRoleService from '../user-role/service/user-role.service'
import jwt from 'jsonwebtoken';
 
let secretKeyHash = `${process.env.PASSWORD_HASH_SECRET_KEY}` || "abc"
@Service()
class AuthController {

    // constructor(private userService: UserService) {}
 
    async login(req: Request, res: Response) {
        try {
            const userServiceInstance = Container.get(UserService);

            const { email, password } = req.body

            const user = await userServiceInstance.findAndCondition({ email: email })

            if (!user) {
                return new ResponseConfig(2000,null,'User is not in DB').ResponseSuccess(req,res)
            }

            let bytes = CryptoJS.AES.decrypt(user.password, secretKeyHash)

            let originalText = bytes.toString(CryptoJS.enc.Utf8)

            if (password !== originalText) {
                return new ResponseConfig(2000,null,'Bạn đã sai password').ResponseSuccess(req,res)
            }
            const access_token = AuthUtils.createAccessToken({ id: user.user_id })
            const refresh_token = AuthUtils.createRefreshToken({ id: user.user_id })

            res.cookie('refreshtoken', refresh_token, {
                httpOnly: true,
                path: '/api/v1/auth/refreshtoken',
                maxAge:  2 * 24 * 60 * 60 * 1000 // 30days
            })

            return res.status(200).json({
                msg: 'Login Success!',
                data:{
                    user,
                    access_token
                },
                statusCode:1000
            })

        } catch (err) {
          return res.status(500).json({ msg: 'Error creating user'+err.message, data: null ,statusCode:2000});
        }
    }
    async register(req: Request, res: Response) {
        try {
            const userServiceInstance = Container.get(UserService)
            const userRoleServiceInstance = Container.get(UserRoleService);
            
            const { email, password, username, phone } = req.body
    
            const user = await userServiceInstance.findOrCondition({email, phone} )

            if (user) {
                return new ResponseConfig(2000,user,'User đã tồn tại vui lòng sử dụng email/phone khác !!!').ResponseSuccess(req,res)
            }
           
            let passwordHash = CryptoJS.AES.encrypt(password, secretKeyHash).toString()
    
            let userNew = await userServiceInstance.create({ email, password: passwordHash, username, phone })

            let userRole = {role_id:2,user_id:userNew.user_id} // Role Customer

            await userRoleServiceInstance.create( userRole)
    
            const access_token = AuthUtils.createAccessToken({ id: userNew.user_id })
            const refresh_token = AuthUtils.createRefreshToken({ id: userNew.user_id })
    
            res.cookie('refreshtoken', refresh_token, {
                httpOnly: true,
                path: '/api/v1/auth/refreshtoken',
                maxAge:  2 * 24 * 60 * 60 * 1000 // 30days
            })
 
            return res.status(200).json({
                statusCode: 1000,
                msg: 'Register Success!',
                access_token,
                user: {
                    userNew,
                    password: ''
                }
            })
          } catch (err) {
            // res.status(500).json({ message: 'Error creating user', error: err.message });
            return new ResponseConfig(2000,null,err.message).ResponseSuccess(req,res)
          }
    }


    async refreshToken (req: Request, res: Response)  {
        try {
          const refreshToken = req.cookies.refreshtoken;
      
          if (!refreshToken) {
            return res.status(401).json({ message: 'Refresh token is required' });
          }
      
          // Xác thực refresh token
          jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN!, (err:any, decoded:any) => {
            if (err) {
              return res.status(403).json({ message: 'Invalid or expired refresh token' });
            }
    
            // Nếu refresh token hợp lệ và còn thời hạn thì generate accesstoken 
            const newAccessToken = AuthUtils.createAccessToken({ id: decoded.id })// Kí lại access_token với id được giải mã
      
            return res.status(200).json({
              message: 'Access token refreshed',
              access_token: newAccessToken,
            });
          });
        } catch (error) {
          return res.status(500).json({ message: 'Error refreshing token', error: error.message });
        }
      };
}
wrapWithLogging(AuthController.prototype, 'login')
wrapWithLogging(AuthController.prototype, 'register')
export default new AuthController()



