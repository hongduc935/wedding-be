import  { Service } from 'typedi'
import  {UserModel} from '../../index'
import { Op } from 'sequelize'
@Service()
class UserService {
  async create(data:any) {
    return await UserModel.create(data)
  }

  async createBulk(data:any[]) {
    return await UserModel.bulkCreate(data)
  }

  async getById(id: number) {
    return await UserModel.findByPk(id)
  }

  async update(id: number, data: any) {
    const user = await UserModel.findByPk(id)
    if (user) {
      return await user.update(data)
    }
    return null 
  }

  async delete(id: number) {
    const user = await UserModel.findByPk(id)
    if (user) {
       await user.destroy()
       return true
    }
    return null
  }

  async findAndCondition(condition: { [key: string]: any }) {
    return await UserModel.findOne({
      where: {
        [Op.and]: condition
      }
    });
  }

  async findOrCondition(condition: { [key: string]: any }) {
    return await UserModel.findOne({
      where: {
        [Op.or]: condition
      }
    });
  }
}

export default UserService;
