import { DataTypes, Model } from 'sequelize'
import sequelize from '../../../modules/databases/mariadb'
import { v4 as uuidv4 } from 'uuid'
import moment from 'moment'



class UserModel extends Model {
  public user_id!: string;
  public username!: string;
  public email!: string;
  public phone!: string;
  public password!: string;
  public created_at!: string;//Thời điểm tạo 
  public updated_at!: string;// Thời điểm cập nhật 
  public deleted_at!: string;// Thời điểm xóa  (nếu có)
}

UserModel.init(
  {
    user_id: {
      type: DataTypes.UUID,
      defaultValue: () => uuidv4(),
      primaryKey: true,
      field: 'USER_ID', 
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'USERNAME', // Đặt tên trường là 'USER_ID'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'EMAIL', // Đặt tên trường là 'USER_ID'
      validate: {
          isEmail: {
              msg: 'Email không hợp lệ!',
          },
      },
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'PHONE', // Đặt tên trường là 'PHONE'
      validate: {
          len: {
              args: [10, 10],
              msg: 'Số điện thoại phải từ 10 ký tự!',
          },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'PASSWORD', // Đặt tên trường là 'USER_ID'
    },
    created_at: {
      type: DataTypes.DATE,
      defaultValue: DataTypes.NOW,
      allowNull: true,
      field: 'CREATED_AT', 
      get() {
        const rawValue = this.getDataValue('created_at');
        return moment(rawValue).format('YYYY-MM-DD HH:mm:ss');
      },
    } ,
    updated_at: {
        type: DataTypes.DATE,
        defaultValue: DataTypes.NOW,
        allowNull: true,
        field: 'UPDATED_AT', 
        get() {
          const rawValue = this.getDataValue('updated_at');
          return moment(rawValue).format('YYYY-MM-DD HH:mm:ss');
        },
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DELETED_AT' 
  },
  },
  {
    sequelize,
    modelName: 'UserModel', // Tên model
    tableName: 'USER', // Tên bảng trong DB
    timestamps: false,
    hooks: {
      beforeCreate: (user: UserModel) => {
        user.user_id = uuidv4(); // Đảm bảo UUID được tạo trước khi tạo bản ghi
      },
      beforeUpdate:(instance, options) => {
        instance.setDataValue('updated_at', new Date());
      },
    },
    indexes: [
        {
            unique: true,
            fields: ['EMAIL'],
        },
        {
            unique: true,
            fields: ['PHONE'],
        },
    ],
  }
);

export default UserModel;
