import { Container, Service } from 'typedi'
import { Response, Request } from 'express'
import UserService from './service/auth.service'
import ResponseConfig from '../../config/response-http'

const userServiceInstance = Container.get(UserService)

@Service()
class UserController {

    async create (req:Request, res:Response) {
        try {
          const record = await userServiceInstance.create(req.body)
          return new ResponseConfig(1000,record,'User created successfully').ResponseSuccess(req,res)
        } catch (err) {
          return new ResponseConfig(2000,null,'Error system in [ UserController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async createBulk (req:Request, res:Response) {
        try {
            const {data} = req.body
            const records = await userServiceInstance.createBulk(data)
            return new ResponseConfig(1000,records,'User created successfully').ResponseSuccess(req,res)
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ UserController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getOne (req:Request, res:Response)  {
        try {
          const newUser = await userServiceInstance.create(req.body)

          console.log("User created successfully")
          res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ UserController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async getProfile (req:Request, res:Response)  {
        try {
            
            const useReq = req['user']

            const user = await userServiceInstance.findOrCondition({email:useReq.email})

            return new ResponseConfig(1000,user,'User created successfully').ResponseSuccess(req,res)
            
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ UserController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }

    async update  (req:Request, res:Response)  {
        try {
            const {user_id,user_name} = req.body 

            const options = {
                user_name
            }
            const newUser = await userServiceInstance.update(user_id,options)
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ UserController ] : [create]' + err.message).ResponseSuccess(req,res)
        }
    }
    async delete  (req:Request, res:Response) {
        try {
            const newUser = await userServiceInstance.create(req.body)

            console.log("User created successfully")
            res.status(201).json({ message: 'User created successfully', data: newUser })
        } catch (err) {
            return new ResponseConfig(2000,null,'Error system in [ UserController ] : [create]' + err.message).ResponseSuccess(req,res) 
        }
    }
}

export default new UserController()



