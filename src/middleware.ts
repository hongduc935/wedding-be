import express from 'express' 
import morgan from 'morgan'
import bodyParser from  "body-parser"
import cors from 'cors'
import compression from  'compression'
import helmet from "helmet"

function CombineAppMiddleWare (app:any){
    const corsOptions = {
        methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH'],
        "origin": "*",
        "preflightContinue": false,
        "optionsSuccessStatus": 204
    };
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(cors(corsOptions));
    app.use(morgan('dev'))
    app.use(express.json());
    // app.use(express.urlencoded())
    app.use(express.urlencoded({ extended: true }));
    app.use(compression({
        level: 6,
        threshold: 100 * 1000
    }))
    app.use(helmet())
}

export default CombineAppMiddleWare