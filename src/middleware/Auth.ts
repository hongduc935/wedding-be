import { Request, Response, NextFunction } from "express"
import jwt from 'jsonwebtoken'
import CombineValidation from '../helpers/validation'
import UserService from "../apis/auth/service/auth.service"
import { Container } from 'typedi'
import UserRoleService from "../apis/user-role/service/user-role.service"
const userServiceInstance = Container.get(UserService)
const userRoleServiceInstance = Container.get(UserRoleService)
const AuthMiddleware = {
    register: (req: Request, res: Response, next: NextFunction) => {
        const { email, password, phone }: any = req.body

        let errValidate: string[] = []

        if (CombineValidation.validateEmail(email) === null) {
            errValidate.push("Email is not validator")
        }

        let isValidatePassword = CombineValidation.validatePassword(password)

        let isValidatePhone = CombineValidation.validatePhone(phone)

        isValidatePassword.length > 0 && errValidate.push(isValidatePassword)

        isValidatePhone.length > 0 && errValidate.push(isValidatePhone)

        if (errValidate.length > 0) return res.status(500).json({
            msg: "validate Error",
            err: errValidate
        })
        next();
    },
    login: (req:Request, res:Response, next:NextFunction) => {
        const { email, password } = req.body

        let errValidate: string[] = []

        if (CombineValidation.validateEmail(email) === null) {
            errValidate.push("Email is not validator")
        }

        let isValidatePassword = CombineValidation.validatePassword(password)

        isValidatePassword.length > 0 && errValidate.push(isValidatePassword)

        if (errValidate.length > 0) return res.status(500).json({
            msg: "validate Error",
            err: errValidate
        })
        console.log("validate success")
        next();

    },

    auth: async (req:any, res:Response, next:NextFunction) => {
        try {
            const token = req.header("Authorization")

            if (!token) return res.status(400).json({ msg: "Token not found." })

            const access_token = token.split(' ')[1]

            const decoded:any = jwt.verify(access_token, `${process.env.JWT_SECRET}`)

            if (!decoded) return res.status(400).json({ msg: "Invalid Authentication." })

            const user = await userServiceInstance.getById( decoded.id )

            // Phân quyền 
            if(user){
                req.user = user // Process user qua cho controller
                next()
            }
            else{
                // Token không hợp lệ  hoặc hết hạn gửi lại response cho client yêu cầu refreshtoken 
                return res.status(403).json({
                    msg: "No permission with action.",
                    err: null
                })
            }
       
        } catch (err:any) {
            console.log(err.message)
            return res.status(500).json({ msg: err.message })
        }
    }
}

export default AuthMiddleware
