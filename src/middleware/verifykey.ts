import {NextFunction, Request, Response} from "express"

const validatorHeaderRequest = async (req:Request,res:Response, next:NextFunction) => {
    try {
        const app_key = req.header("app_key")
        const app_id = req.header("app_id")
        if(!app_key && !app_id ){
            return res.status(400).json({
                msg:"Please provide app_key or app_id"
            })
        }
        if(app_key !== `${process.env["APP_KEY"]}`){
            return res.status(400).json({
                msg:"Please provide app_key or app_id"
            })
        }
        if(app_id !== `${process.env["APP_ID"]}`){
            return res.status(400).json({
                msg:"Please provide app_key or app_id"
            })
        }
        next()
    } catch (err) {
        return res.status(500).json({msg: err.message})
    }
}


export default validatorHeaderRequest