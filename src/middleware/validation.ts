

const validationParams = (req:any,res:any)=>{
    try{
        const params:any = req.params
        for(const key in params){
            if( params[key] === undefined || params[key].length === 0 || params[key] === null ){
                return null 
            }
        }
    }catch(err){

    }
}


const validationQuery = (req:any,res:any)=>{
    try{
        const params:any = req.query;

        for(const key in params){
            if( params[key] === undefined || params[key].length === 0 || params[key] === null ){
                return null 
            }
        }


    }catch(err){

    }
}


const validationBody = (req:any,res:any)=>{
    try{
        const params:any = req.body;

        for(const key in params){
            if( params[key] === undefined || params[key].length === 0 || params[key] === null ){
                return null 
            }
        }


    }catch(err){

    }
}

const validationInput = {
    validationBody,
    validationQuery,
    validationParams
}
export default validationInput

