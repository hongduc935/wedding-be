import { Request, Response, NextFunction } from 'express'
import { Container } from 'typedi'
import Logger from '../config/logger'

const RequestLogger = (req: Request, res: Response, next: NextFunction) => {

    const logger = Container.get(Logger);
    logger.info(`[ START REQUEST ]: --------------------------------------------------------------------------------------------------------------------------`)
    logger.info(`[ Incoming request ]: [${req.method}] [${req.url}]`)
    logger.info(`[ Request headers ] : ${JSON.stringify(req.headers)}`)
    logger.info(`[ Request params ]: ${JSON.stringify(req.params)}`)
    logger.info(`[ Request query ]: ${JSON.stringify(req.query)}`)
    logger.info(`[ Request body ]: ${JSON.stringify(req.body)}`)
    next()
}

const ErrorLogger = (err: Error, req: Request, res: Response, next: NextFunction) => {
    const logger = Container.get(Logger)
    logger.error(`[ERROR] Error occurred during processing request: ${err.message}`)
    res.status(500).send('Internal Server Error')
};

const LoggerMiddeware = {
    RequestLogger,
    ErrorLogger
}


export default LoggerMiddeware
