import "reflect-metadata"
import  dotenv from 'dotenv'
import express from 'express' 
import CombineAppMiddleWare from './middleware'
import http from 'http'
import CombineRouter from './router/index'
import LoggerMiddeware from "./middleware/logger"
import path from "path"
import routerUpload from './modules/file/routers/upload.router'

// Variable inviroment
dotenv.config()
const app = express()

  
// Middleware
CombineAppMiddleWare(app)

// logger request applied 
app.use(LoggerMiddeware.RequestLogger)

// Cấu hình upload file 
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api', routerUpload);

//router
app.use('/api/v1/', CombineRouter)
app.all("*")

// logger error applied 
app.use(LoggerMiddeware.RequestLogger)

// database

const ConfigServer = http.createServer(app)



export default ConfigServer

