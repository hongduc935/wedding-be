import ConnectMongoDB from './src/modules/databases/mongodb';
import sequelize from './src/modules/databases/mariadb';
import ConfigServer from './src/index';
import initSocket from './src/modules/socket/socket'


const server = ConfigServer;

initSocket(server);

const PORT = process.env.PORT || 4000;

function runServer(){
    sequelize.sync({ alter: true }).then(() => { 
        server.listen(PORT, () => {
          console.log(`Server running on port ${PORT}, process ID: ${process.pid}`);
        });
      }).catch((error) => {
        console.error('Unable to connect to the database:', error);
      });
 
    ConnectMongoDB()  
}  
  
  
export default runServer   

 